# importing webdriver from selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import time
from PIL import Image
# reference: https://coderzcolumn.com/tutorials/python/imaplib-simple-guide-to-manage-mailboxes-using-python
import imaplib
import email
import os
import re
import requests

TOKEN = "1840725479:AAF6_zFFj82TTns1l60HosNIIspiRWOIVF01234567"
chat_id = "-10017363036731234567"

def read_email():

    ################ IMAP SSL ##############################

    with imaplib.IMAP4_SSL(host="imap.gmail.com", port=imaplib.IMAP4_SSL_PORT) as imap_ssl:

        ############### Login to Mailbox ######################
        resp_code, response = imap_ssl.login("stock_alert_scan@gmail.com", "jmevecnhdriwinxoabcde")

        ############### Set Mailbox #############
        resp_code, mail_count = imap_ssl.select(mailbox="INBOX", readonly=True)

        ############### Search mails in a given Directory #############
        resp_code, mails = imap_ssl.search(None, '(FROM "alerts@thinkorswim.com")')

        mail_ids = mails[0].decode().split()

        # Get last element by accessing element at index -1 (-2: from 2nd last to last) - sorted(mail_ids[-2:], reverse=True) last 2 elements from last to second last element)
        for mail_id in mail_ids[-1:]:

            resp_code, mail_data = imap_ssl.fetch(mail_id, '(RFC822)') ## Fetch mail data.

            message = email.message_from_bytes(mail_data[0][1]) ## Construct Message from mail data
            #print("\n")
            #print("Subject    : {}".format(message.get("Subject")))
            msg = message.get("Subject")
            # Output is multiline:
            # Alert: Following list of symbols were added to Stoch14BUp80: CARG, 
            # DKS, IBM.
            remove_newlines = ("".join(msg.splitlines()))
            #print(remove_newlines)
            
            #Get only words after certain word in the line
            Stoch14BUp80 = "Stoch14BUp80:"
            symbols_only = remove_newlines[remove_newlines.index(Stoch14BUp80) + len(Stoch14BUp80):]
            
            # remove . from output: IBM, AAPL.
            str_symbols = (re.sub("\.","",symbols_only))
            # remove all white spaces
            str_symbols_1 = (re.sub(r"\s+", "", str_symbols)) 


            message = "Daily Symbols Getting Ready for a Breakout: " + str_symbols_1
            url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={message}"
            requests.get(url).json() # this sends the message
            
            symbol_list = list(str_symbols_1.split(","))
            return(symbol_list)
            
        ############# Close Selected Mailbox #######################
        imap_ssl.close()
    
def send_screenshot():
    list_of_symbols = read_email()

    for symbol in list_of_symbols:

        # Here Chrome  will be used
        driver = webdriver.Chrome(executable_path='C:/Users/juriz/Desktop/chromedriver.exe')
        driver.implicitly_wait(0.5)

        time.sleep(1)


        driver.set_window_size(1160, 860)
        #print(driver.get_window_size())
        #driver = webdriver.Chrome()
        # URL of website
        url = "https://www.tradingview.com/chart/"

        # Opening the website
        driver.get(url)
        time.sleep(3)

        click_search_stock = driver.find_element_by_css_selector("#header-toolbar-symbol-search > .js-button-text")
        click_search_stock.click()
        time.sleep(3)

        stock = driver.find_element_by_css_selector(".search-eYX5YvkT")
        stock.send_keys(symbol)
        stock.send_keys(Keys.ENTER)

        time.sleep(5)

        driver.save_screenshot('C:/chart-images/image1.png')

        time.sleep(2)
        # Loading the image
        #image = Image.open(r'C:\chart-images\image1.png')

        os.system('curl -s -X POST "https://api.telegram.org/bot1840725479:AAF6_zFFj82TTns1l60HosNIIspiRWOIVF01234567/sendPhoto" -F chat_id="-10017363036731234567" -F photo="@C:/chart-images/image1.png"')
        time.sleep(3)
        """
        # Showing the image
        image.show()
        image.close()

        driver.close() # To Close the driver connection
        driver.quit() # To Close the browser
        """
        driver.close()

read_email()
send_screenshot()
