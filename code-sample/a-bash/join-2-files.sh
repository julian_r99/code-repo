#!/bin/bash

# Create a 3 file with only a few elements of file1 & file2

#file1 example:
#server1 offline=true
#file2 example
#server1 dept_name=networking vm_name=vm1

#file3 should have:
#server1 offline=true dept_name=networking vm_name=vm1

C=0

IFS=$'\n' read -d '' -r -a f1 < /path-to-file1
IFS=$'\n' read -d '' -r -a f2 < /path-to-file2

line_count=$(cat /path-to-file1 | wc -l)

while [ $C -lt ${line_count} ];
do
    f1line=$(echo ${f1[$C]})
    f2line=$(echo ${f2[$C]})
    svr=$(echo $f1line | awk '{print $1}')
    svr_status=$(echo $f1line | awk '{print $2}')
    dept=$(echo $f2line | awk '{print $2}')
    vm=$(echo $f2line | awk '{print $3}')
    echo "$svr $svr_statu $dept $vm" >> /path-to-file3
    C=$((C+1))
done
