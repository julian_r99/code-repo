#!/usr/bin/python

# Given /proc/mounts ex: /dev/vda1 / ext4 rw,relatime,errors=remount-ro,data=ordered 0 0, 
# we will parse the 1st element as passed arg (ex: /dev/vda1) and store mount point to test the write operation

import subprocess
from argparse import ArgumentParser

mnt_pnts=[]
testfile='fs-check-test-deleteme.txt'
proc_mounts='/proc/mounts'
mnt_keys=[]

parser = ArgumentParser()
parser.add_argument('-m', '--mnt_keys', nargs='+', default=[], required=True, help='List of filesystems to be parse. (Ex: "/dev/vda1" "/dev/mapper" "mydomain.com"')
args = parser.parse_args()

for item in args.mnt_keys:
    mnt_keys.append(item) 


with open(proc_mounts,'r') as f:
    eachline = [line.split() for line in f.readlines()]

for line in eachline:
    for fs in mnt_keys:
        if fs in line[0]:
            mnt_pnts.append(line[1])

for mnt in mnt_pnts:
    cmd='touch {}/{}'.format(mnt, testfile)
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate()
    if p.returncode != 0: 
       print("FAILURE: FS {} Read-only. Write opeation Error Code: {}, error: {}".format(mnt, p.returncode, error))
    else:
       print("SUCCESS: FS {} is writeble".format(mnt))
    cmd='rm {}/{}'.format(mnt, testfile)
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate()
