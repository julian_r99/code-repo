import re
import json
import pprint
import sys
from pathlib import Path

'''
prometheus targets.json file:
[
  {
    "targets": [ "localhost:9100", "host2:9100", "host3:9100" ],
    "labels": {
      "env": "prod",
      "job": "node"
    }
  }
]
'''

new_server=" {{ new_server }}:9100 "

jsonFile = Path("./targets.json")
if jsonFile.is_file():
    pass
else:
    print "ERROR: jsonFile ./targets.json does NOT exist"
    sys.exit(1)


with open(jsonFile, 'r') as f:
    try:
        data = json.load(f)
        data[0]["targets"].append(new_server)
    except ValueError, e:
        print ("JSON object issue: %s") % e
        sys.exit(1)

uniqueServers = []
for server in data[0]["targets"]:
    if(server not in uniqueServers):
        uniqueServers.append(server)

# deleting the targets element to re-add only unique servers
for element in data:
    element.pop('targets', None)

# adding uniqueServers
data[0]["targets"] = uniqueServers

with open(jsonFile, 'w') as f:
    try:
        f.write(json.dumps(data, indent=4, sort_keys=True))
    except ValueError, e:
        print ("JSON write error: %s") % e
        sys.exit(1)

