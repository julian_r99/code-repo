#!/bin/bash

CURRENT_DIR=$(echo $PWD)
STATUS_FILE_LONG="${CURRENT_DIR}/status_long.txt"
STATUS_FILE_SHORT="${CURRENT_DIR}/status_short.txt"

TODAY=$(date +"%Y-%m-%d")
STATUS_LONG_DATE=$(cat ${STATUS_FILE_LONG} |grep -P '^\d')
STATUS_SHORT_DATE=$(cat ${STATUS_FILE_SHORT} |grep -P '^\d')

LONG_TRIGGERED=$(cat ${STATUS_FILE_LONG} |grep -Pv "^\d" | grep -v "not" | grep -Pv "^=" | tr '\n' ',' ) # need to join newlines into single line to send to telegram
SHORT_TRIGGERED=$(cat ${STATUS_FILE_SHORT} |grep -Pv "^\d" | grep -v "not" | grep -Pv "^=" | tr '\n' ',')

TOTAL_SYMBOLS=$(cat ${STATUS_FILE_LONG} | grep -P '^=' | awk -F '=' '{print $2}')
TOTAL_SYMBOLS_LONG=$(cat ${STATUS_FILE_LONG} | grep -Pv '^\d'| grep -Pv '^=' | awk -F '_' '{print $1}' | wc -l)
TOTAL_SYMBOLS_SHORT=$(cat ${STATUS_FILE_SHORT} | grep -Pv '^\d' | grep -Pv '^=' | awk -F '_' '{print $1}' | wc -l)
BOT=""
CHATID=""
CURL_CMD="https://api.telegram.org/bot${BOT}/sendMessage?chat_id=${CHATID}&text"

if [ -z "${STATUS_LONG_DATE}" ]
then
      #${STATUS_LONG_DATE} is NULL - setting to bogus number for comparison below
      STATUS_LONG_DATE=1
fi
if [ -z "${STATUS_SHORT_DATE}" ]
then
      #${STATUS_SHORT_DATE} is NULL - setting to bogus number for comparison below
      STATUS_SHORT_DATE=1
fi

LONG() {
    if (( "${TODAY}" != "${STATUS_LONG_DATE}" )); then
        curl -s "$CURL_CMD=Status_file_long_not_updated"
    elif [ -z "${LONG_TRIGGERED}" ]; then
        curl -s "$CURL_CMD=No_Long_Triggers_Today"
    elif [ ! -z "${LONG_TRIGGERED}" ]; then
        curl -s "$CURL_CMD=$LONG_TRIGGERED"
    fi

}

SHORT() {
    if (( "${TODAY}" != "${STATUS_SHORT_DATE}" )); then
        curl -s "$CURL_CMD=Status_file_short_not_updated"
    elif [ -z "${SHORT_TRIGGERED}" ]; then
        curl -s "$CURL_CMD=No_Short_Triggers_Today"
    elif [ ! -z "${SHORT_TRIGGERED}" ]; then
        curl -s "$CURL_CMD=$SHORT_TRIGGERED"
    fi

}

SYMBOL_COUNT() {
    if (( "${TOTAL_SYMBOLS_LONG}" != "${TOTAL_SYMBOLS}" )); then
        #%0A within '' are the same as \n for new lines
        LONG_MSG='Total_Symbols_Long does not match. %0AFound='
        LONG_MSG+="$TOTAL_SYMBOLS_LONG vs Correct=$TOTAL_SYMBOLS"
        curl -s "$CURL_CMD=$LONG_MSG"
    fi
    if (( "${TOTAL_SYMBOLS_SHORT}" != "${TOTAL_SYMBOLS}" )); then
        SHORT_MSG='Total_Symbols_Short does not match. %0AFound='
        SHORT_MSG+="$TOTAL_SYMBOLS_SHORT vs Correct=$TOTAL_SYMBOLS"
        curl -s "$CURL_CMD=$SHORT_MSG"
    fi
}

LONG
echo "\n"
SHORT
echo "\n"
SYMBOL_COUNT
echo "\n"
