dns_host={
    "svr01" : {
       "search_domains" : ['myns1.com', 'myns2'],
       "nameserver" : ['1.1.1.1', '1.1.1.2'],
       "bind_user" : 'myuser01',
       "bind_pw" : 'MyPass_01',
       "base_dn" : 'dc=mydc01,dc=local'
    },
    "svr02" : {
       "search_domains" : ['myns1.com', 'myns2'],
       "nameserver" : ['1.1.1.1', '1.1.1.2'],
       "bind_user" : 'myuser02',
       "bind_pw" : 'MyPass_02',
       "base_dn" : 'dc=mydc02,dc=local'
    }
}
