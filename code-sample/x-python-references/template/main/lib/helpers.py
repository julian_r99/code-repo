#!/usr/bin/python

import os, sys, time, re, pprint

def rd_file(filename):
    file = open(filename, 'r')
    out = file.read()
    file.close()
    return out

def wr_file(filename, buf):
    file = open(filename, 'w')
    file.write(buf)
    file.close()

def path_to_root():
    root = re.search('(.*)/template/', __file__).group(1)
    return root

def path_to_playbooks():
    path = path_to_root()
    return '{}/ansible/playbooks'.format(path)
