eval "$(rbenv init -)"
export PATH="$HOME/.rbenv/shims:$PATH"

#export GITHUB_HOST=  add git hub host here is wanted

export PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:$PATH
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*


if [ -f ~/.git-completion.bash ]; then
  source ~/.git-completion.bash
fi
c_cyan=`tput setaf 6`
c_l_cyan=`tput bold; tput setaf 6`
c_red=`tput setaf 1`
c_pink=`tput bold; tput setaf 1`
c_green=`tput setaf 2`
c_l_green=`tput bold; tput setaf 2`
c_yellow=`tput setaf 3`
c_l_yellow=`tput bold; tput setaf 3`
c_blue=`tput setaf 4`
c_purple=`tput setaf 5`
c_sgr0=`tput sgr0`
 
parse_git_branch ()
{
  if git rev-parse --git-dir >/dev/null 2>&1
  then
    gitver=$(git branch 2>/dev/null| sed -n '/^\*/s/^\* //p')
  else
    return 0
  fi
    echo -e \($gitver\)
}
 
branch_color ()
{
  if git rev-parse --git-dir >/dev/null 2>&1
  then
    color=""
    if git diff --quiet 2>/dev/null >&2
    then
      color="${c_l_green}"
    else
      color=${c_red}
    fi
  else
    return 0
  fi
  echo -ne $color
}
 
PS1='\[${c_cyan}\]\u\[${c_yellow}\]@\h \[${c_purple}\]\w\[${c_sgr0}\] \[$(branch_color)\]$(parse_git_branch)\[${c_sgr0}\]: '

alias fp='for host in {a,b,c,d,e,f,g}.hostname-of-machines; do  ping -c 1 $host; done'
alias home='cd ~/'
alias ..='cd ..'
eval "$(rbenv init -)"
