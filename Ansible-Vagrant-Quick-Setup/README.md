README
======

Quick example of installing Vagrant and running ansible roles to install miscellaneous packages. 


Requirements
------------
 
Mac OSX with Internet connection, install vagrant (tested version: Vagrant 2.0.0 - https://www.vagrantup.com/docs/installation/),
install VirtualBox (tested version: VirtualBox 5.1.28 r117968 - https://www.virtualbox.org/wiki/Downloads)
Install Ansible (tested version: Ansible 2.4.0 - https://valdhaus.co/writings/ansible-mac-osx/)

Once all is installed, clone this repo with this files and simply run:
```
vagrant up   (once vagrant is up, you can run 'vagrant ssh' to login to your new Vagrant box)
```

Example of running Playbook manually
-----------------------------------
```
ansible-playbook  --connection=ssh --timeout=30 --limit='server01' -i pool/vagrant/hosts -b -u vagrant -v --tags=ntp,hosts basic-setup.yml --private-key=/Users/<mac-username>/.vagrant.d/insecure_private_key
```

References
----------

http://docs.ansible.com/ansible/playbooks_best_practices.html
https://galaxy.ansible.com/

