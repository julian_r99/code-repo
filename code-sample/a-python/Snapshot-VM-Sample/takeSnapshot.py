#!/usr/bin/python 

#This is a sample script that takes values from snapshotConfig and take snapshots

import sys
import datetime
import subprocess
import argparse
from pysphere import VIServer # Tested with vCenter Server 5.5.0 and pysphere package 0.1.7
from snapshotConfig import * # Contains username and password for vCenter connection, list of VM names to take snapshot

# Main function
def takeSnapshot():

        # Get the environment to take snapshot from command line
	parser = argparse.ArgumentParser(description="Take snapshot of VMs for domain environment")
	parser.add_argument('env', choices = ("ca", "ny"), help="Env subdomains Ex: box1.ca.domain.lan, box1.ny.domain.lan")
	env = parser.parse_args().env

	# Connect to vCenter
	vmServer = VIServer()
	vmServer.connect(vcenterurl, username, password)

	# Build the snapshot name to be used - using date with secs to avoid conflict of same names
	date = datetime.datetime.today().strftime('%m-%d-%y--%H:%M:%S') 
        # If we want to add git revision from a server to snapshot name, uncomment next 2 lines
	# revision = subprocess.check_output("ssh -A replace-with-your-user@server01." + env + ".domain.lan 'cd /CodeRepo; git branch -v |grep branchName |awk \"{print \$3}\";'", shell=True)
        #snapshotName = date + "_CodeRev_" + revision.rstrip()
        snapshotName = date 
	print "Using snapshot name: " + snapshotName

	numVms = len(vmList)

	cnt = 0
	for vm in vmList:
		vmName = vm + "." + env + ".domain.lan"
		print "Taking snapshot of: " + vmName

		# Start snapshot of one VM
		task = vmServer.get_vm_by_name(vmName).create_snapshot(snapshotName, sync_run=False)
		cnt += 1

		# For big environments, take snapshot in batches of 6
		if cnt % 6 == 0 or cnt == numVms:

			# Wait till the last VM in the batch is complete
			status = task.wait_for_state(["success", "error"])
			if status == "success":
				print "Snapshot successful"
			else:
				print "Error taking snapshot", task.get_error_message()

	vmServer.disconnect()

if __name__ == "__main__":
	takeSnapshot()
