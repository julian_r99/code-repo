Playbook-grafana-on-influxdb
----------------------------

Will be install of the grafana and influxdb on Ubuntu.

# Prerequired

- [ansible](http://www.ansible.com/) Tested version: 1.9.1
- [vagrant](http://www.vagrantup.com/) Tested version: 1.7.4 
- [virtualbox](https://www.virtualbox.org/) Tested version: 4.3.34 r104062

Mac OSX with Internet connection, install vagrant (tested version: Vagrant 1.7.4 - https://www.   vagrantup.com/docs/installation/),
install VirtualBox (tested version: VirtualBox 4.3.34 r104062 - https://www.virtualbox.org/wiki/  Downloads)
Install Ansible (tested version: Ansible 1.9.5 - https://valdhaus.co/writings/ansible-mac-osx/)

# Connecting on Vagrant box

Once all is installed, clone this repo with this files and simply run:
```
vagrant up   (once vagrant is up, you can run 'vagrant ssh' to login to your new Vagrant box)
```

Example of running Playbook manually
------------------------------------

By default, ```vagrant up``` will spin up a vagrant box and run ansible automatically, but if you want to run commands manually from your Mac where ansible is installed, below is an example:

```
ansible-playbook --user=vagrant --connection=ssh --timeout=30 --limit='trusty' -i inventory --sudo -v deploy_grafana_on_influxdb.yml --private-key=/Users/<YOUR-MAC-USERNAME>/.vagrant.d/insecure_private_key
```

## Grafana on browser

```
> open http://<your-vagrant-box-ip>/  # In our case, our Vagrantfile has the IP set to: 192.168.50.4
```

## influxDB on browser

- username:password is ```root:root```

```
> open http://<your-vagrant-box-ip>:8083/   # In our case, our Vagrantfile has the IP set to: 192.168.50.4
```

References
----------

http://docs.ansible.com/ansible/playbooks_best_practices.html
https://galaxy.ansible.com/
