class MyClass(object):    -> Object is just for good coding but not required.
    RESTRICTION_FILE="vm_restrictions_template.json"   -> This is a constant or var that should not be changed
    def __init__(self, required_var, myvar="abc"): -> initialize required_var and optional vars where optional can have value changed when calling the class, but required_var will always need to be passed when calling the Class.
        self.myvar = myvar
        self.required_var = required_var

    def main(self):
        print “Python Class details”
        print self.RESTRICTION_FILE 

if __name__ == "__main__":
    ob = MyClass(required_var=123) —> Create the object from the class then use the ob to call the methods.
    ob.main()

========================
__init__ it always runs on a call of a Class. It is a good for updating scripts before executing them, gathering information, setting environment variables, etc
Example:
class MyOps(object):
    """My Operation Class comments Example: Get local info for list of servers
           return dict: {host1: {},
                         host2: {}
                         }
    """

    MYSCRIPT = '/usr/local/bin/myscript' # assume script has a update flag that updates itself

    def __init__(self):
        self.ops_getsvrinfo()
        self.myscript_update()

    def ops_getsvrinfo(self):
        ops_scan_bin       = 'tools/bin/scan-myserver.bash'
        
    def myscript_update(self):
        exec_cmd('sudo {} update'.format(self.MYSCRIPT))

========================


from irdo.tools.lib.helpers import (str2list, path_irdo_data)

class IRDO_Policy(object):
    def __init__(self, dryrun=False):
        self.dryrun = dryrun
        

__init__ is function to assign values to object properties and is called automatically every time the class is being used to create a new object.

    def set_env(self):
        fdir = path_irdo_data()
        self.vm_policy = fdir + 'vm_restrictions_template.json'

Self here has 2 meaning:
1-self.vm_policy means that I’m making this variable available within the Class so any other Methods can use it.
2-fdir variable does not have self because It is only available with this set_env method.

If I’m calling a Method from another Class (ex: path_irdo_data) I don’t need self.
=============

class SVR_List(SVR):
    def __init__(self):
        super(SVR_List, self).__init__() # super() alone returns a temporary object of the superclass that then allows you to call that superclass’s methods and inherits variables, etc from the subclass.
                                         # not an expert on this one



