#!/bin/bash

# Simple script that can check the rates of a website and email someone if it is lower than their rate
# You can then configure this script to run every day as a cronjob

wget -q https://www.wellsfargo.com/mortgage/rates/refinance-assumptions?prod=18 > /dev/null 2>&1

currentRate=$(grep intRate refinance-assumptions\?prod\=18 |grep '%' | tail -c 12 | cut -c1-5 |sed 's/\.//g')
myrate=3750

if [ "$currentRate" -lt "$myrate" ] ; then
    echo ' Rate is lower. Current Rate: ' $currentRate
    mail -s 'Rate is lower' myemail@myemail.com << EOF
Current Rate: $currentRate%
EOF
fi

# Clean up downloaded files
rm refinance-assumptions*
