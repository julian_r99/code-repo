#! /usr/bin/python

# useful example of how to store data values in a constant and then
# build a list of process key values as needed

import sys
from argparse import ArgumentParser
sys.path.append('/root/template/main')
from config.server_constants import (dns_host)

parser = ArgumentParser()
parser.add_argument('-H', '--host', default=[], required=True, help='Server hostname.')
args = parser.parse_args()

svr =  args.host

svr_lst = dns_host.keys()
for server in svr_lst:
    print("Server: {}".format(server))


search_domains = dns_host[svr]['search_domains']
nameserver = dns_host[svr]['nameserver']
bind_user = dns_host[svr]['bind_user']
bind_pw = dns_host[svr]['bind_pw']
base_dn = dns_host[svr]['base_dn']

print("search_domains: {}, nameserver: {}, bind_user: {}, bind_pw:{}, base_dn:{}".format(search_domains, nameserver, bind_user, bind_pw, base_dn))
