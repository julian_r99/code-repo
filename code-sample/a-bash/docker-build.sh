#!/bin/bash

# Simple sample of a docker build script. We can make it better by 
# adding push to the Repo once the image is built.

REPO="docker.localdomain.lan/docker-image-name"
GITREV=$(git rev-parse --short=12 HEAD)
IMAGE="$REPO:git-$GITREV"

docker build --rm "$@" -t $IMAGE .
