#!/usr/bin/env python3.6

import requests
import json
import time
import datetime
import sys
import os

API_KEY='VQIPSWMCA8NNMTJRXC'

Symbols=['EURUSD', 'EURGBP', 'EURAUD', 'EURCAD', 'EURCHF', 'EURJPY', 'EURNZD', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'GBPUSD', 'AUDCAD', 'AUDUSD', 'AUDNZD', 'AUDCHF', 'CADJPY', 'CADCHF', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'NZDUSD', 'USDCAD', 'USDCHF', 'USDJPY', 'CHFJPY'] # 27 Symbols until CHFJPY

#Symbols=['EURUSD', 'EURGBP', 'NZDUSD']

SymbolsTotal=(len(Symbols))

current_dir = os.getcwd()
status_file_long = '{}/status_long.txt'.format(current_dir)
status_file_short = '{}/status_short.txt'.format(current_dir)


def trigger_scanner_long(Symbol,API_KEY,Symbol_a,Symbol_b):
    #res=requests.get('https://www.alphavantage.co/query?function=MACD&symbol='+Symbol+'&fastperiod=5&slowperiod=37&signalperiod=7&interval=daily&series_type=close&apikey='+API_KEY)
    count=0
    data=res.json()['Technical Analysis: MACD']

    ordered_dates=[]
    for date in data:
        ordered_dates.append(date)
    ordered_data={}
    for date in ordered_dates[::-1]:
        ordered_data.update({date:data[date]})

    previousK=None
    currentK=None
    previousD=None
    currentD=None
    previousClose=None
    currentClose=None
    triggered_dates=[]
    check_for_close_price=[]

    data=ordered_data

    for date in data:
        currentK=float(data[date]['MACD'])
        currentD=float(data[date]['MACD_Signal'])

        if previousK==None:
            previousK=float(data[date]['MACD'])
            previousD=float(data[date]['MACD_Signal'])

        if currentK>currentD:
            if previousK<previousD:
                if currentK>previousK:
                    triggered_dates.append(date)
                    
        previousK=currentK
        previousD=currentD
        
    triggeredK=None
    triggered_date=None
    previous_date=None
    for date in triggered_dates:
        currentK=float(data[date]['MACD'])
        
        if not triggeredK==None:
            if currentK>triggeredK:
               check_for_close_price.append([date,data[date],{'previousK':triggeredK,'currentK':currentK},previous_date])
               triggeredK=currentK
            else:
                triggeredK=currentK

        else:
            triggeredK=currentK
        previous_date=date
        
    #res2=requests.get("https://www.alphavantage.co/query?function=FX_DAILY&from_symbol="+Symbol_a+"&to_symbol="+Symbol_b+"&outputsize=full&apikey="+API_KEY)
    Final_matches=[]
        
    for each_match in check_for_close_price:
        try:
            data_for_date=res2.json()['Time Series FX (Daily)']
        except:
            print('Exception_long getting data_for_date. items: {}\n '.format(res2.json().items()))

        # each_match[0] is the date and most cases price and indicator data have different historical starting date from alphavantage
        # price: ['2000-06-30']; Indicator: ['2002-02-14']
        if each_match[0] in data_for_date.keys():
            try:
                currentClose=float(data_for_date[each_match[0]]['4. close'])
                currentDate=each_match[0]
                previousDate=triggered_dates[triggered_dates.index(currentDate)-1]
                previousClose=float(data_for_date[previousDate]['4. close'])

                if currentClose<previousClose:
                    if float(each_match[1]['MACD'])<0:
                        Final_matches.append([each_match,{'Previous Close':previousClose,'Current Close':currentClose},previousDate])
                    
            except Exception as e:
                print('Exception_long error {}\n'.format(e))

    last_match_date=Final_matches[-1][0][0]
    print('last_match_date_long: ', last_match_date)
    print('Today date: ', datetime.datetime.now().strftime('%Y-%m-%d'))
    print('\n')
   
    long_status = []
    if last_match_date==datetime.datetime.now().strftime('%Y-%m-%d'):
        long_status.append('{}_triggered_long'.format(Symbol))

    else:
        long_status.append('{}_not_triggered_long'.format(Symbol))
    return long_status

def trigger_scanner_short(Symbol,API_KEY,Symbol_a,Symbol_b):
    count=0
    data=res.json()['Technical Analysis: MACD']

    ordered_dates=[]
    for date in data:
        ordered_dates.append(date)
    ordered_data={}
    for date in ordered_dates[::-1]:
        ordered_data.update({date:data[date]})

    previousK=None
    currentK=None
    previousD=None
    currentD=None
    previousClose=None
    currentClose=None
    triggered_dates=[]
    check_for_close_price=[]

    data=ordered_data

    for date in data:
        currentK=float(data[date]['MACD'])
        currentD=float(data[date]['MACD_Signal'])

        if previousK==None:
            previousK=float(data[date]['MACD'])
            previousD=float(data[date]['MACD_Signal'])

        if currentK<currentD:
            if previousK>previousD:
                if currentK<previousK:
                    triggered_dates.append(date)

        previousK=currentK
        previousD=currentD

    triggeredK=None
    triggered_date=None
    previous_date=None
    for date in triggered_dates:
        currentK=float(data[date]['MACD'])

        if not triggeredK==None:
            if currentK<triggeredK:
               check_for_close_price.append([date,data[date],{'previousK':triggeredK,'currentK':currentK},previous_date])
               triggeredK=currentK
            else:
                triggeredK=currentK

        else:
            triggeredK=currentK
        previous_date=date

    #res2=requests.get("https://www.alphavantage.co/query?function=FX_DAILY&from_symbol="+Symbol_a+"&to_symbol="+Symbol_b+"&outputsize=full&apikey="+API_KEY)
    Final_matches=[]

    for each_match in check_for_close_price:
        try:
            data_for_date=res2.json()['Time Series FX (Daily)']
        except:
            print('Exception_short getting data_for_date. items: {}\n'.format(res2.json().items()))

        # each_match[0] is the date and most cases price and indicator data have different historical starting date from alphavantage
        # price: ['2002-06-30']; Indicator: ['2000-02-14']
        #print(data_for_date.keys())
        #sys.exit(0)
        if each_match[0] in data_for_date.keys():
            try:
                currentClose=float(data_for_date[each_match[0]]['4. close'])
                currentDate=each_match[0]
                previousDate=triggered_dates[triggered_dates.index(currentDate)-1]
                previousClose=float(data_for_date[previousDate]['4. close'])

                if currentClose>previousClose:
                    if float(each_match[1]['MACD'])>0:
                        Final_matches.append([each_match,{'Previous Close':previousClose,'Current Close':currentClose},previousDate])

            except Exception as e:
                print('Exception_short error {}\n'.format(e))

    last_match_date=Final_matches[-1][0][0]
    print('last_match_date_short: ', last_match_date)
    print('Today date: ', datetime.datetime.now().strftime('%Y-%m-%d'))

    short_status = []
    if last_match_date==datetime.datetime.now().strftime('%Y-%m-%d'):
        short_status.append('{}_triggered_short'.format(Symbol))

    else:
        short_status.append('{}_not_triggered_short'.format(Symbol))
    return short_status

# Starting with a Clean file
with open(status_file_long, 'w') as f:
    f.write(datetime.datetime.now().strftime('%Y-%m-%d'))
    f.write('\n')
    f.write('={}'.format(SymbolsTotal))
    f.write('\n')
with open(status_file_short, 'w') as f:
   f.write(datetime.datetime.now().strftime('%Y-%m-%d'))
   f.write('\n')
   f.write('={}'.format(SymbolsTotal))
   f.write('\n')

# Main execution
for Symbol in Symbols:
    Symbol_a = Symbol[0:3]
    Symbol_b = Symbol[3:6]
    res=requests.get('https://www.alphavantage.co/query?function=MACD&symbol='+Symbol+'&fastperiod=5&slowperiod=37&signalperiod=7&interval=daily&series_type=close&apikey='+API_KEY)
    res2=requests.get("https://www.alphavantage.co/query?function=FX_DAILY&from_symbol="+Symbol_a+"&to_symbol="+Symbol_b+"&outputsize=full&apikey="+API_KEY)    
    print("==================================================")
    print('Data for the symbol {}, Symbol_a {}, Symbol_b {}\n'.format(Symbol, Symbol_a, Symbol_b))

    mesg_returned_long=trigger_scanner_long(Symbol,API_KEY,Symbol_a,Symbol_b)
    for items in mesg_returned_long:
        with open(status_file_long, 'a') as f:
            f.write(items)
            f.write('\n')

    mesg_returned_short=trigger_scanner_short(Symbol,API_KEY,Symbol_a,Symbol_b)
    for items in mesg_returned_short:
        with open(status_file_short, 'a') as f:
            f.write(items)
            f.write('\n')

    print('{} {}'.format(mesg_returned_long, mesg_returned_short))
    print('\n')
    time.sleep(31)

