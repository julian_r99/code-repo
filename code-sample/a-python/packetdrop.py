#!/usr/bin/python

with open('packetdrop.txt', 'r') as f:
	    drops = {}
	    for line in f:
		    if 'drops' in line:
			    date, time, ip, fn, n, packets = line.split()
	                    drops[ip] = drops.get(ip, 0) + int(n)
for ip, count in drops.items():
	print ip, count


