#!/bin/bash

# Simple script to checkout the latest git release branch on a server that have a git checkedout, update its code,
# verify config file, then update the DB

echo " "
echo "===>Step 1 Updating code on server01"
cd /git_repo_dir; git remote update; git checkout -B dev_master dev/master # assuming this default values (change as needed)
echo " "
echo "Server01 Code Updated"
echo " "

echo "===>Step 2 Verifying config file has Env set to development, then updating DB"

MY_HOST="server01.mydomain.lan"
MY_ENV="development"
FILE="/home/juliano/juliano-dir/db-output.log"
WORD="done."

Y=`hostname -f`
X=`grep environment= /my_env.conf | cut -d "=" -f 2`
# my_env.conf looks like this below. grep command will grep environment=, cut on delimiter = and keep 2 field
# environment=development

if [ "$X" != "$MY_ENV" ] || [ "$Y" != "$MY_HOST" ]; then
	echo " "
	echo "Exiting. Current WH ENV $X or host $Y must be $MY_ENV and $MY_HOST in order to update DB"
	exit 1
else
	echo " "
	echo "Host ->$MY_HOST<- and Env ->$MY_ENV<- are correct. Updating DB"
	date > /home/juliano/juliano-dir/db-output.log # logging time stamp everytime before updating db
	/git_repo_dir/scripts/UpdateMyDB.pl >> /home/juliano/juliano-dir/db-output.log # saving output for checking if needed

	if grep -q "$WORD" "$FILE"; then
		echo " " 
		echo "DB Update succeeded"
	else
		echo " "
		echo "Exiting. DB Update failed"
	fi
fi
