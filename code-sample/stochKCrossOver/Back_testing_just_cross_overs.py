import requests
import json
import time
import datetime


API_KEY='XYZVQIPSWMCA8NNMTJRXYZ'
Symbols=['spy']


def trigger_scanner(Symbol,API_KEY):
    res=requests.get('https://www.alphavantage.co/query?function=STOCH&symbol='+Symbol+'&fastkperiod=14&slowkperiod=3&slowdperiod=3&interval=daily&apikey='+API_KEY)

    count=0
    data=res.json()['Technical Analysis: STOCH']

    ordered_dates=[]
    for date in data:
        ordered_dates.append(date)
    ordered_data={}
    for date in ordered_dates[::-1]:
        ordered_data.update({date:data[date]})
        
        

    previousK=None
    currentK=None
    previousD=None
    currentD=None
    previousClose=None
    currentClose=None
    
    triggered_dates=[]
    check_for_close_price=[]

    data=ordered_data

    for date in data:
        currentK=float(data[date]['SlowK'])
        currentD=float(data[date]['SlowD'])

        if previousK==None:
            previousK=float(data[date]['SlowK'])
            previousD=float(data[date]['SlowD'])
            previousDate=date

        if currentK>currentD:
            if previousK<previousD:
                if currentK>previousK:
                    #Adding currentK value so I only want crosses > 70
                    if currentK>70:
                        triggered_dates.append([date,previousDate])
                    
        previousK=currentK
        previousD=currentD
        previousDate=date



    res2=requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+Symbol+"&outputsize=full&apikey="+API_KEY)
    Final_matches=[]

    totalProfit=[]
    totalLoss=[]

    try:
        data_for_date=res2.json()['Time Series (Daily)']

        for each_date in triggered_dates[::-1][:3]: # last 3 cross overs
            try:
                currentClose=float(data_for_date[each_date[0]]['4. close'])
                previousLow=float(data_for_date[each_date[1]]['3. low'])
                
    
                next_dates=ordered_dates[::-1][ordered_dates[::-1].index(each_date[0])+1:]
                
                stopLoss = previousLow
                profitTarget = (currentClose - previousLow) * 2.0 + currentClose

                for next_date in next_dates:
                    if float(data_for_date[next_date]['4. close'])>=profitTarget:
                        print('previousLow: ',previousLow,'profitTarget: ',round(float(profitTarget),2),'  currentClose: ',currentClose,'  Exit Close Price: ',float(data_for_date[next_date]['4. close']))
                        print("*Profit Hit: triggered date is ",each_date[0],'close price on triggered date',currentClose,'Profit hit on ',next_date, 'close price on profit date',float(data_for_date[next_date]['4. close']))
                        totalProfit.append(round(float(data_for_date[next_date]['4. close'])-currentClose,2))
                        print('\n')
                        break

                    elif float(data_for_date[next_date]['4. close'])<=stopLoss:
                        print('previousLow: ',previousLow,'stopLoss: ',round(float(stopLoss),2),'  currentClose: ',currentClose,'  Exit Close Price: ',float(data_for_date[next_date]['4. close']))
                        print("*Loss Hit: triggered date is ",each_date[0],'close price on triggered date',currentClose,'Loss hit on ',next_date, 'close price on loss date',float(data_for_date[next_date]['4. close']))
                        totalLoss.append(round(currentClose-float(data_for_date[next_date]['4. close']),2))
                        print('\n')
                        break

                    
            
            except Exception as e:
                print(e)
                
        print('Each Profit      :',totalProfit,' ==> total Profit        :'  ,round(sum(totalProfit),2))
        print('Each Loss         :',totalLoss,' ==> total Loss        :'  ,round(sum(totalLoss),2))
        
    except:
        print(res2.json().items())
        


for Symbol in Symbols:
    print('Data for symbol ', Symbol)
    
    trigger_scanner(Symbol,API_KEY)

    print('\n\n\n')
    
