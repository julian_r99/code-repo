#!/usr/bin/python

# Ping a host, ssh and check log

import subprocess
import sys, re
sys.path.append('/root/template/main')
# set Server Env PYTHONPATH instead of hacky above 
# echo 'export PYTHONPATH=$PYTHONPATH:/root/template/main' >> ~/.bashrc
from config.constants import (VAR_LOG_MSGS, PING_COUNT, ADMIN_HOST)
from helpers import (path_to_root, path_to_playbooks)

class MyClass(object):
    def exec_cmd_exp(self):
        cmd = 'sudo ssh {} ls {}'.format(ADMIN_HOST, VAR_LOG_MSGS)
        try:
            out = subprocess.check_output(cmd, shell=True)
            print out
        except Exception as exc:
            print 'Command failed: {}'.format(exc)

        root_path = path_to_root()
        print root_path   
        ansible = path_to_playbooks()
        print ansible
        


if __name__ == "__main__":
    ob = MyClass()
    ob.exec_cmd_exp()
