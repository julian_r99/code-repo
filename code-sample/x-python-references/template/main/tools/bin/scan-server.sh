#!/usr/bin/env bash
#
# Scan servers for information on resources
#

THISSCRIPT="$(basename $0)"

function Usage() {
cat << EOF
Usage: $THISSCRIPT   [ -h ] [ -c ]  [ -n ]  [ -v ]

-h      Help text

-c      Give output in csv form

-n      Names of fields (columns) are printed at top,
        when printing in csv format

-v      Report VMs instead of hypervisors

EOF
exit $1
}

get_mem_total()
{
    _MemTotal_=`grep MemTotal /proc/meminfo | head -1 | awk '{print $2}'`
    _MemUnit_=`grep MemTotal /proc/meminfo | head -1 | awk '{print $3}'`
    unit_convert_gb $_MemTotal_ $_MemUnit_ mem_size_gb
}

csvmode=0
vmmode=0
fieldnames=0

while getopts ":chnv" opt; do
    case $opt in
        c) csvmode=1
           ;;
        h) Usage 0
           ;;
        n) fieldnames=1
           ;;
        v) vmmode=1
           ;;
        *) Usage 1
           ;;
    esac
done

kernel=$(uname -r)
hostname=$(hostname -s)

cpu_model="$(dmidecode -t processor | grep Version | sed -e 's/^.*Version:[ ]\+//' | head -1)"
core_count="$(lscpu | grep '^CPU(s):' | awk '{print $2}')"

get_mem_total

swaptotal_k_or="$(grep '^SwapTotal:' /proc/meminfo | awk '{print $2}')"
swaptotal_k=$((swaptotal_k_or/1024000))
swapfree_k="$(grep '^SwapFree:' /proc/meminfo | awk '{print $2}')"
swapuse_k=$(( ${swaptotal_k} - ${swapfree_k} ))


if [[ $csvmode -ne 0 ]]; then
    if [[ $fieldnames -ne 0 ]]; then
        echo "hostname,cpu_total,mem_total,swap_total,VM_number,cpu_usage,mem_usage,native_vlan,switchport,VMs"
    fi
    echo "\"${hostname}\",${core_count},${mem_size_gb},${swaptotal_k},${vm_count},${vm_cpu_total},${vm_mem_total},\"${native_vlan}\",\"${switchport}\",\"$(echo ${vm_list})\""
else
    echo "{"
    echo "    "\"hostname\":\"${hostname}\",
    echo "    "\"cpu_total\":${core_count},
    echo "    "\"mem_total\":${mem_size_gb},
    echo "    "\"swap_total\":${swaptotal_k},
    echo "    "\"VM_number\":${vm_count},
    echo "    "\"cpu_usage\":${vm_cpu_total},
    echo "    "\"mem_usage\":${vm_mem_total},
    echo "    "\"native_vlan\":\"${native_vlan}\",
    echo "    "\"switchport\":\"${switchport}\",
    echo "    "\"VMs\":\"${vm_list}\",
    echo "    "\"old_style_vm\":\"${old_style}\"
    echo "}"
fi
