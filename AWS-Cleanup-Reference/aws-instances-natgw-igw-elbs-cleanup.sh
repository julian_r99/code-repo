#!/bin/bash

# AWS random services cleanup 

usage() { echo "Usage: $0 <vpc-id> (example: $0 vpc-123456"; exit 2; }

[[ $# -ne 1 ]] && usage

VPC_ID=$1

# Deleting Instances
aws ec2 describe-instances --filters "Name=vpc-id,Values=${VPC_ID}" |grep -i InstanceId | awk '{print $2}' | sed 's/,//g' | xargs aws ec2 terminate-instances --instance-ids

# Deleting Nat-Gateway
aws ec2 describe-nat-gateways --filter "Name=vpc-id,Values=${VPC_ID}" |grep NatGatewayId |awk '{print $2}' | sed 's/,//g' | xargs aws ec2 delete-nat-gateway --nat-gateway-id

# Release Public IP from nat-gateway
# NOTE: Nat-Gateway takes forever (10-20 mins) to disappear from AWS Console, therefore, this will fail
# Have it down for reference
# aws ec2 describe-nat-gateways --filter "Name=vpc-id,Values=${VPC_ID}" |grep AllocationId |awk '{print $2}' | sed 's/,//g' | xargs aws ec2 release-address --allocation-id

# Internet gateway also has other public IPs that we can not find in AWS Console or because it still shows up in NAT-Gateway so cmd fails too
# Having it as reference too:
# aws ec2 describe-internet-gateways --filters "Name=attachment.vpc-id,Values=${VPC_ID}" |grep igw | awk '{print $2}' | sed 's/,//g' | xargs aws ec2 detach-internet-gateway --vpc-id ${VPC_ID} --internet-gateway-id
 
# ELB Deletions:
for ELB in `aws elb describe-load-balancers |grep ${VPC_ID} |grep LoadBalancerName | awk '{print $2}' | sed 's/,//g' | sed 's/"//g'`; 
do 
	aws elb delete-load-balancer --load-balancer-name "${ELB}"; 
done

