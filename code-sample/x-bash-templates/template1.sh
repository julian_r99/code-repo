#!/bin/bash

VAR_1="$1"
VAR_2="$2"

error() {
    echo "$*"
    exit 1
}

if [ "${1}NULL" == "NULL" ] || [ "${2}NULL" == "NULL" ]; then
    error "usage: $0 <VAR_1> <VAR_2>"
fi

pre_check() {
    echo "some check"
    echo ""
}

main_operation() {
    echo "Pre-check passed"
    echo "Perform main operation"
    echo ""
}

verification() {
    echo "Verify main operation got correct result"
}

pre_check
main_operation
verification
