#!/usr/bin/python

import sys
import subprocess
import shlex
import time
import argparse
import re

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description="""Wrapper script to run ansible, get realtime output, write output to logfile and
parse output to write new inventory file with success/failure server list""",
    epilog="""
Example Usage: ansible_wrapper.py -c "sudo ansible-playbook playbook.yml -e target=server_list" -i "hosts"

Example failure lines capture:
ex: server1.mydomain.com : ok=0    changed=0    unreachable=1    failed=0
ex: server2.mydomain.com : ok=0    changed=0    unreachable=0    failed=1

Example new_inventory_file:
[server_list]
#success server1
#failed server2
server3
everything else left without comment""")

parser.add_argument('-c', '--cmd', default=[], required=True, help='Linux command (Ex: "sudo ansible-playbook playbook.yml -e target=server1"')
parser.add_argument('-i', '--inventory', default=[], required=True, help='Ansible inventory file under inventory directory (Ex: "hosts"')

args = parser.parse_args()

timestr = time.strftime("%Y-%m-%d_%H-%M-%S")
log_output = "{}_log_output_{}_{}.txt".format(sys.argv[0], timestr, args.inventory)
new_inventory = "{}_new_inventory_{}_{}.txt".format(sys.argv[0], timestr, args.inventory)
command = "{}".format(args.cmd)
hosts = " -i inventory/{}".format(args.inventory)
command += hosts
error_list = [] 
success_list = []
failed_hosts = []
success_hosts = []
new_host_list = []
hosts_file = "inventory/{}".format(args.inventory)
hostList = [line.rstrip('\n') for line in open(hosts_file)] # put all lines as single element in a list


def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # clear file before start writing to it
    with open(log_output, 'w'): pass
    with open(new_inventory, 'w'): pass
    # getting-realtime-output
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            print output.strip()
            with open(log_output, "a+") as log:
                log.write(output.strip() + "\n")
    rc = process.poll()
    
    print("**************** Ansible Completed  ****************")
    print("---->log_output: {}".format(log_output))
    print("---->new_inventory: {}".format(new_inventory))
    print("---->Command: {}\n".format(command))

    if int(rc) != 0:
        print("=========================")
        print("ERRORS found.")
        print("ERROR: cmd: {}".format(command))
        print("Return Code {}.".format(rc))
        print("=========================\n")

    with open(log_output, "r") as log:
        content = log.read().splitlines()
        for line in content:
            if "unreachable=" not in line and "failed=" not in line:
                continue
            if "unreachable=0" in line and "failed=0" in line:
                success_list.append(line)
            else:
                error_list.append(line)
    return rc

def new_inventory_file():
    for svr in error_list:
        failed_hosts.append(svr.split()[0]) # getting only server name ex: error_list= ['server1              : ok=0    changed=0    unreachable=1    failed=0']
    
    for svr in success_list:
        success_hosts.append(svr.split()[0])
    
    for svr1 in hostList:
        if svr1 in failed_hosts:
            new_host_list.append("#failed {}".format(svr1))
        if svr1 in success_hosts:
            new_host_list.append("#success {}".format(svr1))
        if svr1 not in failed_hosts and svr1 not in success_hosts:
            new_host_list.append("{}".format(svr1))

    # write it out to new_inventory_file
    with open(new_inventory, "a+") as f:
        for item in new_host_list:
            f.write(item.strip() + "\n") 
 
    
if __name__ == "__main__":
   run_command(command)
   new_inventory_file()
