README
======

Role to deploy Jenkins


Simple playbook/vagrant setup to install docker and deploy jenkins container
----------------------------------------------------------------------------

Mac OSX with Internet connection, install vagrant (tested version: Vagrant 2.0.0 - https://www.vagrantup.com/docs/installation/), install VirtualBox (tested version: VirtualBox 5.1.28 r117968 - https://www.virtualbox.org/wiki/Downloads) Install Ansible (tested version: Ansible 2.4.0.0 - https://valdhaus.co/writings/ansible-mac-osx/)
Once all is installed, clone this repo with this files and simply run:

```
vagrant up   (once vagrant is up, you can run 'vagrant ssh' to login to your new Vagrant box)

```

To Access Jenkins
-----------------

http://<vagrant-ip>:8080   # Our Vagrantfile has ip set to: 192.168.50.4

Example of running Playbook manually
-----------------------------------
```
ansible-playbook  --connection=ssh --timeout=30 --limit='server01' -i pool/vagrant/hosts -b -u vagrant -v --tags=jenkins basic-setup.yml --private-key=/Users/<mac-username>/.vagrant.d/insecure_private_key
```
