#!/bin/bash

CI_MASTER_URL="$1"
SLAVE="$2"
JENKINS_USER="$3"
JENKINS_PW="$4"

error() {
    echo "$*"
    exit 1
}

if [ "${1}NULL" == "NULL" ] || [ "${2}NULL" == "NULL" ] || [ "${3}NULL" == "NULL" ] || [ "${4}NULL" == "NULL" ]; then
    error "usage: $0 <CI_MASTER_URL> <SLAVE> <JENKINS_USER> <JENKINS_PW>"
fi

offline_node() {
    OFFLINE=$(curl -s "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/api/json" | grep -Po '"offline":true')

    if [ "${OFFLINE}" != '"offline":true' ]; then
        echo "Slave ${SLAVE} is online. Putting it offline"
        OUT=$(curl -s -X POST -u ${JENKINS_USER}:${JENKINS_PW} "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/toggleOffline")
        AUTH_ERROR=$(echo "${OUT}" |grep -i "HTTP ERROR")
        if [ "${AUTH_ERROR}NULL" == "NULL" ]; then
            echo "SUCCESS: Offline Node"
        else
            echo "FAIL: Offline Node failed due to Authentication"
            exit 1
        fi
    else
        echo "Node already Offline. Proceeding."
    fi
}


online_node() {
    echo "Ensure Node ${SLAVE} does not have any more jobs running before putting Node Back Online"
    sleep 10
    while true ; do
        RUNNING_JOB=$(curl -s "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/api/json" | grep -Po '"idle":true')
        if [[ "${RUNNING_JOB}" == '"idle":true' ]]; then
            echo "Slave ${SLAVE} finish running Job. Putting Node Back Online"
            break
        fi
    done

    echo "Putting Node Back Online"
    curl -s -X POST -u ${JENKINS_USER}:${JENKINS_PW} "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/toggleOffline"
    echo ""
    echo "===>NOTE: Putting Node Online re-launchs the SlaveAgent.<==="
    echo ""
    echo "===>If jobs running:"
    echo "Within 1-2 mins Slave will go from Offline to Broken Connection to Back Online for: ${SLAVE}"
    sleep 10
    echo ""
    # launchSlaveAgent operation below not needed per my testing. Leaving it for reference
    #sleep 10
    #curl -s -X POST -u ${JENKINS_USER}:${JENKINS_PW} "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/launchSlaveAgent"
}

verify_node_online() {
    echo "Verifying Node is Online"
    ONLINE=$(curl -s "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/api/json" | grep -Po '"offline":false')
    if [ "${ONLINE}" != '"offline":false' ]; then
        echo "Node not Online. Putting it back Online"
        curl -s -X POST -u ${JENKINS_USER}:${JENKINS_PW} "http://${CI_MASTER_URL}:8080/computer/${SLAVE}/toggleOffline"
    else
        echo "Node already Online. Done"
        echo ""
    fi
}

offline_node
online_node
verify_node_online
