#!/usr/bin/env python3.6

import requests
import json
import time
import datetime
import Email_sender
import sys

API_KEY='VQIPSWMCA8NNMTJRXC'

Symbols=['EURUSD', 'GPBUSD', 'USDJPY', 'USDCAD', 'USDCHF', 'NZDUSD', 'AUDUSD', 'AUDNZD', 'AUDCAD', 'AUDJPY', 'XAUUSD', 'EURCAD', 'NZDJPY', 'GBPJPY', 'XAGUSD', 'GBPCHF', 'EURNZD', 'EURJPY', 'EURGBP', 'EURCHF', 'EURAUD', 'CHFJPY', 'CADJPY', 'CADCHF', 'AUDCHF']


def trigger_scanner(Symbol,API_KEY,Symbol_a,Symbol_b):

    #res=requests.get('https://www.alphavantage.co/query?function=MACD&symbol='+Symbol+'&fastperiod=5&slowperiod=37&signalperiod=7&interval=daily&series_type=close&apikey='+API_KEY)
    with open('eurusd-macd5-37-7.json') as data_file1:
        res = json.load(data_file1)

        count=0
        #data=res.json()['Technical Analysis: MACD']
        data=res['Technical Analysis: MACD']

        ordered_dates=[]
        for date in data:
            ordered_dates.append(date)
        ordered_data={}
        for date in ordered_dates[::-1]:
            ordered_data.update({date:data[date]})

        previousK=None
        currentK=None
        previousD=None
        currentD=None
        previousClose=None
        currentClose=None
        triggered_dates=[]
        check_for_close_price=[]

        data=ordered_data

        for date in data:
            currentK=float(data[date]['MACD'])
            currentD=float(data[date]['MACD_Signal'])

            if previousK==None:
                previousK=float(data[date]['MACD'])
                previousD=float(data[date]['MACD_Signal'])

            if currentK>currentD:
                if previousK<previousD:
                    if currentK>previousK:
                        triggered_dates.append(date)
                        
            previousK=currentK
            previousD=currentD
            
        triggeredK=None
        triggered_date=None
        previous_date=None
        for date in triggered_dates:
            currentK=float(data[date]['MACD'])
            
            if not triggeredK==None:
                if currentK>triggeredK:
                   check_for_close_price.append([date,data[date],{'previousK':triggeredK,'currentK':currentK},previous_date])
                   triggeredK=currentK
                else:
                    triggeredK=currentK

            else:
                triggeredK=currentK
            previous_date=date
        
    #res2=requests.get("https://www.alphavantage.co/query?function=FX_DAILY&from_symbol="+Symbol_a+"&to_symbol="+Symbol_b+"&outputsize=full&apikey="+API_KEY)
    with open('eurusd-price.json') as data_file2:
        res2 = json.load(data_file2)
        Final_matches=[]
            
        for each_match in check_for_close_price:
            try:
                data_for_date=res2['Time Series FX (Daily)']
            except:
                print('Exception getting data_for_date. items: '.format(res2.items()))

            # each_match[0] is the date and most cases price and indicator data have different historical starting date from alphavantage
            # price: ['2000-06-30']; Indicator: ['2002-02-14']
            if each_match[0] in data_for_date.keys():
                try:
                    currentClose=float(data_for_date[each_match[0]]['4. close'])
                    currentDate=each_match[0]
                    previousDate=triggered_dates[triggered_dates.index(currentDate)-1]
                    previousClose=float(data_for_date[previousDate]['4. close'])
                    #print('Debug: currentClose: {}, currentDate: {}, previousDate: {}, previousClose: {}\n'.format(currentClose, currentDate, previousDate, previousClose))       

                    if currentClose<previousClose:
                        if float(each_match[1]['MACD'])<0:
                            #print('Print float(each_match[1]: {}.\n'.format(float(each_match[1]['MACD'])))
                            #print('Debug of Final_matches.append. Output below.')
                            #print([each_match,{'Previous Close':previousClose,'Current Close':currentClose},previousDate])
                            #print('\n')
                            Final_matches.append([each_match,{'Previous Close':previousClose,'Current Close':currentClose},previousDate])
                        
                except Exception as e:
                    print('Exception error {}'.format(e))

        #print('JR')
        #print(Final_matches[-1]) # This will print nothing if we hit exception above
        #print(Final_matches)
        #sys.exit(0)
        # End JR #
        last_match_date=Final_matches[-1][0][0]
        print('last_match_date: ', last_match_date)
        print('Today date: ', datetime.datetime.now().strftime('%Y-%m-%d'))

        mesg=''
        if last_match_date==datetime.datetime.now().strftime('%Y-%m-%d'):
            mesg='Trigger found for '+'"'+Symbol+'"'+'\n\n'+'Details for the trigger'+'\n\n'+str(Final_matches[-1])

        else:
            mesg='No triggers today for '+'"'+Symbol+'"'+' last_match_date: '+'"'+last_match_date+'"'+'\n\n'
            #mesg='No triggers today for '+'"'+Symbol+'"'+' here are the last 3 trigger details'+'\n\n'
            #for i in [1,2,3]:
                #mesg=mesg+str(Final_matches[::-1][i-1])+'\n\n'
        return mesg
        
def mail_data(trig_data):
#    gmailUser='email@gmail.com'
#    gmailPassword='XXXXX'
#    recipient='email@gmail.com'
    message=trig_data
    Email_sender.send_mail(gmailUser,gmailPassword,recipient,message)

# Need to append all the mesg to a list and then send in 1 email instead of one for each
for Symbol in Symbols:
    Symbol_a = Symbol[0:3]
    Symbol_b = Symbol[3:6]
    print('Data for the symbol {}, Symbol_a {}, Symbol_b {}\n'.format(Symbol, Symbol_a, Symbol_b))
    mesg_returned=trigger_scanner(Symbol,API_KEY,Symbol_a,Symbol_b)
    #mail_data(mesg_returned)
    print(mesg_returned)
    print('\n')
    time.sleep(1)

