#!/usr/bin/env python3.6

import requests
import json
import time
import datetime
import Email_sender

API_KEY='VQIPSWMCA8NNMTJRXC'
#Symbols=['UUP','FXE','USO','DNO','SPY','SH','IWM','RWM','TBT','TLT','GLD','DGLD','FXA','FXC','WEAT','SOYB','CORN','NIB','JO','SGG']
Symbols=['FXE']

def trigger_scanner(Symbol,API_KEY):

    #res=requests.get('https://www.alphavantage.co/query?function=STOCH&symbol='+Symbol+'&fastkperiod=10&slowkperiod=6&slowdperiod=6&interval=daily&apikey='+API_KEY)
    res=requests.get('https://www.alphavantage.co/query?function=MACD&symbol='+Symbol+'&fastperiod=10&slowperiod=16&signalperiod=9&interval=daily&series_type=close&apikey='+API_KEY)



    count=0
    data=res.json()['Technical Analysis: MACD']

    ordered_dates=[]
    for date in data:
        ordered_dates.append(date)
    ordered_data={}
    for date in ordered_dates[::-1]:
        ordered_data.update({date:data[date]})
        
        

    previousK=None
    currentK=None
    previousD=None
    currentD=None
    previousClose=None
    currentClose=None
    triggered_dates=[]
    check_for_close_price=[]

    data=ordered_data

    for date in data:
        currentK=float(data[date]['MACD'])
        currentD=float(data[date]['MACD_Signal'])

        if previousK==None:
            previousK=float(data[date]['MACD'])
            previousD=float(data[date]['MACD_Signal'])

        if currentK>currentD:
            if previousK<previousD:
                if currentK>previousK:
                    triggered_dates.append(date)
                    
        previousK=currentK
        previousD=currentD

        
    triggeredK=None
    triggered_date=None
    previous_date=None
    for date in triggered_dates:
        currentK=float(data[date]['MACD'])
        
        if not triggeredK==None:
            if currentK>triggeredK:
               check_for_close_price.append([date,data[date],{'previousK':triggeredK,'currentK':currentK},previous_date])
               triggeredK=currentK
            else:
                triggeredK=currentK

        else:
            triggeredK=currentK

        previous_date=date


        
    res2=requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+Symbol+"&outputsize=full&apikey="+API_KEY)
    Final_matches=[]

        
    for each_match in check_for_close_price:
        try:
            data_for_date=res2.json()['Time Series (Daily)']
        except:
            print(res2.json().items())

        try:
            currentClose=float(data_for_date[each_match[0]]['4. close'])
            currentDate=each_match[0]
            previousDate=triggered_dates[triggered_dates.index(currentDate)-1]
            previousClose=float(data_for_date[previousDate]['4. close'])
            
            if currentClose<previousClose:
                if float(each_match[1]['MACD'])<0:
                    Final_matches.append([each_match,{'Previous Close':previousClose,'Current Close':currentClose},previousDate])

          
            
        except Exception as e:
            print(e)



    last_match_date=Final_matches[-1][0][0]
    print('last_match_date: ', last_match_date)
    print('Today date: ', datetime.datetime.now().strftime('%Y-%m-%d'))

    mesg=''
    if last_match_date==datetime.datetime.now().strftime('%Y-%m-%d'):
        mesg='Trigger found for '+'"'+Symbol+'"'+'\n\n'+'Details for the trigger'+'\n\n'+str(Final_matches[-1])

    else:
        mesg='No triggers today for '+'"'+Symbol+'"'+' last_match_date: '+'"'+last_match_date+'"'+'\n\n'
        #mesg='No triggers today for '+'"'+Symbol+'"'+' here are the last 3 trigger details'+'\n\n'
        #for i in [1,2,3]:
            #mesg=mesg+str(Final_matches[::-1][i-1])+'\n\n'

    
    return mesg

        
def mail_data(trig_data):
    gmailUser='stockalertscan@gmail.com'
    gmailPassword='Stock@lert_100'
    recipient='stockalertscan@gmail.com'
    message=trig_data

    Email_sender.send_mail(gmailUser,gmailPassword,recipient,message)


# Need to append all the mesg to a list and then send in 1 email instead of one for each
for Symbol in Symbols:
    print('Data for the symbol   : '+Symbol+'\n')
    mesg_returned=trigger_scanner(Symbol,API_KEY)
    #mail_data(mesg_returned)
    print(mesg_returned)
    print('\n')
    time.sleep(3)



