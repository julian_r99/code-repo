#!/bin/bash
DAILY=$(cat /etc/crontab |grep "/etc/cron.daily")
if [ $? -eq 0 ]; then
    TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
    cp /etc/crontab /etc/crontab.${TIMESTAMP}
    D_MINUTE=$(echo $(expr $RANDOM % 59))
    D_HOUR=$(echo $(expr $RANDOM % 23))
    cat /etc/crontab |grep -v "/etc/cron.daily" > /tmp/crontab_daily
    echo "# DEVOPS-4930 Randomizing cron.daily/weekly" >> /tmp/crontab_daily
    echo "${D_MINUTE} ${D_HOUR} * * * root test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )" >> /tmp/crontab_daily
    # Check if weekly is also defined and generate new random hour and minute
    WEEKLY=$(cat /tmp/crontab_daily |grep "/etc/cron.weekly")
    if [ $? -eq 0 ]; then
        W_MINUTE=$(echo $(expr $RANDOM % 59))
        W_HOUR=$(echo $(expr $RANDOM % 23))
        cat /tmp/crontab_daily |grep -v "/etc/cron.weekly" > /tmp/crontab_weekly
        echo "${W_MINUTE} ${W_HOUR} * * 7 root test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )" >> /tmp/crontab_weekly
        cp /tmp/crontab_weekly /etc/crontab
        rm /tmp/crontab_weekly /tmp/crontab_daily
    else
        cp /tmp/crontab_daily /etc/crontab
        rm /tmp/crontab_daily
    fi
fi
