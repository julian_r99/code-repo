#!/usr/bin/python

# Creating this script to checkout the latest prod branch into my svn sandbox 
# Then Checkout my sandbox in order to test a given PP (Patch Priority) with a dev ticket


import subprocess
import os

print 'Getting latest Prod version - Running svn log --stop-on-copy  https://svn-repo.url/svn/branches/prod --limit 1 \n'

p1 = subprocess.Popen(['svn', 'log', '--stop-on-copy', 'https://svn-repo.url/svn/branches/prod', '--limit', '1'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
svnout1, err = p1.communicate()
print svnout1 + '\n'

prodver = svnout1.split("\n")[1].split("|")[0].strip()[1:]
print 'This is the current Prod version ' + prodver + '\n'

#print 'Copying prod branch revision', prodver, 'to my sandbox \n'

pptic = raw_input('Enter PP ticket as dev1234: ')
print '\n'
print 'Creating tmp dir with name /var/tmp/prod_' + prodver + '_' + pptic + '\n'

tmpdir = '/var/tmp/prod_' + prodver + '_' + pptic

p2 = subprocess.Popen(['mkdir', '/var/tmp/prod_'+ prodver + '_' + pptic], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
svnout2, err = p2.communicate()

print 'Changing directory to ' + tmpdir + '\n'
os.chdir(tmpdir)

print 'Checking out https://svn-repo.url/svn/sandbox/juliano/prod_' + prodver + '_' + pptic  + ' into current directory' + '\n'

p3 = subprocess.Popen(['svn', 'co', 'https://svn-repo.url/svn/sandbox/juliano/prod_42174_DEV1234', './'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
svnout3, err = p3.communicate()
