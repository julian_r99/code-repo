#!/usr/bin/python3
# reference: https://coderzcolumn.com/tutorials/python/imaplib-simple-guide-to-manage-mailboxes-using-python
import imaplib
import email
import os

current_dir = os.getcwd()
daily_result = (current_dir + "/daily_result.txt")

################ IMAP SSL ##############################

with imaplib.IMAP4_SSL(host="imap.gmail.com", port=imaplib.IMAP4_SSL_PORT) as imap_ssl:
    #print("Connection Object : {}".format(imap_ssl))

    ############### Login to Mailbox ######################
    #print("Logging into mailbox...")
    resp_code, response = imap_ssl.login("stock_alert_scan@gmail.com", "jmevecnhdriwinxoabcde")

    #print("Response Code : {}".format(resp_code))
    #print("Response      : {}\n".format(response[0].decode()))

    ############### Set Mailbox #############
    resp_code, mail_count = imap_ssl.select(mailbox="INBOX", readonly=True)

    ############### Search mails in a given Directory #############   
    resp_code, mails = imap_ssl.search(None, '(FROM "alerts@thinkorswim.com")')

    mail_ids = mails[0].decode().split()

    #print("Total Mail IDs : {}\n".format(len(mail_ids)))
    

    # Get last element by accessing element at index -1 (-2: from 2nd last to last) - sorted(mail_ids[-2:], reverse=True) last 2 elements from last to second last element)
    for mail_id in mail_ids[-1:]:
        #print("================== Start of Mail [{}] ====================".format(mail_id))

        resp_code, mail_data = imap_ssl.fetch(mail_id, '(RFC822)') ## Fetch mail data.

        message = email.message_from_bytes(mail_data[0][1]) ## Construct Message from mail data
        print("\n")
       # print("From       : {}".format(message.get("From")))
       # print("To         : {}".format(message.get("To")))
       # print("Bcc        : {}".format(message.get("Bcc")))
       # print("Date       : {}".format(message.get("Date")))
        #print("Subject    : {}".format(message.get("Subject")))
        msg = message.get("Subject")

        with open(daily_result, 'w') as out:
            out.write(msg + '\n')
        
        os.system('var=`cat /Users/jrizzi/Desktop/daily_result.txt`; curl -s -X POST "https://api.telegram.org/bot1840725479:AAF6_zFFj82TTns1l60HosNIIspiRWOIVF01234567/sendMessage" -d chat_id="-10017363036731234567" -d text="$var"')

        #print(msg)
        #print("\n")

       # print("Body : ")
       # for part in message.walk():
       #     if part.get_content_type() == "text/plain": ## Only Printing Text of mail. It can have attachements
       #         body_lines = part.as_string().split("\n")
       #         print("\n".join(body_lines[:6])) ### Print first few lines of message
       # print("================== End of Mail [{}] ====================\n".format(mail_id))

  

    ############# Close Selected Mailbox #######################
    imap_ssl.close()
