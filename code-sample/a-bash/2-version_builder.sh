#!/bin/bash

# Simple script to increment the build revision 
# Current revision is 1.2.3 where 2 and 3 can only go as high as 9
# Examples: 
# 1.1.1 => 1.1.2
# 1.1.9 => 1.2.0
# 1.9.9 => 2.0.0

# Function for printing usage
usage() { echo "Usage: $0 version (example: 1.1.1)"; exit 2; }

# One parameter must be passed
[[ $# -ne 1 ]] && usage

version=$1
highest_ver=9
increment=1

# Getting all 3 subversion numbers
ver1=`echo $version |awk 'BEGIN{FS="."} {print $1}'`
ver2=`echo $version |awk 'BEGIN{FS="."} {print $2}'`
ver3=`echo $version |awk 'BEGIN{FS="."} {print $3}'`

# Starting from last subversion number - if != 9, just increment and leave ver1 ver2 the same
if [ $ver3 -ne $highest_ver ]; then
    new_ver3=$((ver3 + increment))
    x="$ver1.$ver2.$new_ver3"
    echo $x

# If ver3 is 9, I leave ver1 same, increment ver2 and set ver3 to 0
else
    if [ $ver2 -ne $highest_ver ]; then
        new_ver2=$((ver2 + increment))
        y="$ver1.$new_ver2.0"
        echo $y

# If ver2 is 9, I increment ver1 and set ver2 and ver3 to 0
    else
        new_ver1=$((ver1 + increment))
        z="$new_ver1.0.0"
        echo $z
    fi
fi