#!/bin/bash

THISSCRIPT="$(basename $0)"

function Usage() {
cat << EOF
Script to check Jenkins Node if Offline and if No jobs running.
*This assumes you have a file containing jenkins-name : jenkins-node


Usage: $THISSCRIPT  [ -n <JENKINS_NODE> -n <JENKINS_NODE> ... ]

-n <JENKINS_NODE> Jenkins Slave Node (ex: NODE1 or NODE2) 

EOF
exit $1
}

unset MYARR

IDX=0

while getopts "hn:" opt; do
    case ${opt} in
        h) Usage 0
           ;;
        n) MYARR[${IDX}]="${OPTARG}" 
           ((IDX++))
           ;;
        \?) echo "Invalid option: -${OPTARG}" >&2
           Usage 1
           ;;
    esac
done

shift $((OPTIND-1))

main() {
    SVRS=$(cat alljenkins-servers.txt | grep -E "\b${JENKINS_NODE}\b" | tr -d ' ')
    JENKINS=$(echo ${SVRS} |awk -F ':' '{print $1}')

    if [ "${JENKINS}NULL" == "NULL" ]; then
        echo "ERROR: Jenkins_Node: ${JENKINS_NODE} is not found in any Jenkins."
        echo ""
    else 
        OFFLINE=$(curl -s http://${JENKINS}:8080/computer/${JENKINS_NODE}/api/json | jq '.offline')
        IDLE=$(curl -s http://${JENKINS}:8080/computer/${JENKINS_NODE}/api/json | jq '.idle')
         
        if [ "${OFFLINE}" = "true" ]; then 
            echo "Jenkins Slave Node: ${JENKINS_NODE} found on Jenkins: ${JENKINS} and is currently Offline"
            echo "Jenkins_Node: ${JENKINS_NODE} Offline status: ${OFFLINE}"
            echo ""
        else
           echo "WARNING: Jenkins Slave Node: ${JENKINS_NODE} found on Jenkins: ${JENKINS} and is currently NOT Offline"
           echo "Jenkins_Node: ${JENKINS_NODE} Offline status: ${OFFLINE}"
           echo ""
        fi 
        if [ "${IDLE}" = "true" ]; then
            echo "Jenkins Slave Node: ${JENKINS_NODE} is NOT running jobs"
            echo "Idle status: ${IDLE}"
            echo ""
        else
           echo "WARNING: Jenkins Slave Node: ${JENKINS_NODE} has running jobs"
           echo "Idle status: ${IDLE}"
           echo ""
        fi
    fi
}

for ITEM in "${MYARR[@]}"
do 
    JENKINS_NODE="${ITEM}"
    echo "====== Status Check for ${JENKINS_NODE} ======="
    main
done
