#!/usr/bin/python

# This is a simple script to submit a Post request 
# If you are running from a Mac OS X and you may not have 'requests'
# you will have to install them. An easy way to install using pip is: sudo pip install requests

import requests
import urllib

params = urllib.urlencode({'@number': 23496, '@type': 'issue', '@action': 'show'})
headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

r = requests.post('http://bugs.python.org', params, headers)

print r.status_code
print r.url


