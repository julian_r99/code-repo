#!/bin/bash

SVR_LIST="$1"

error() {
    echo "$*"
    exit 1
}

if [ "${1}NULL" == "NULL" ]; then
    error "usage: $0 <Server_List_File>"
fi

for SVR in `cat ${SVR_LIST}`; do
    CPU=$(sudo ssh ${SVR} cat /proc/cpuinfo |grep processor | wc -l);
    MEM=$(free -h | grep Mem | awk '{print $2}')
    printf "Svr: %s - Cpu_Count: %s - Mem: %s\n" "${SVR}" "${CPU}" ${MEM};
done
# output example:
# Svr: Server1 - Cpu_Count: 40 - Mem: 503G
