# 1-Given string that contain only digits, determine if all 2-character
# substrings '00', '01', .., '99' are present
# 2-Binary Search Tree

import sys

def hasall(s):

	n = 2
	lst1 = []

	for i in range(0, len(s), n): 
		out = s[i:i + n] 
		lst1.append(out)
	
	lst2 = []
	for ii in range(len(s)/2):
		lst2.append('%02d' % ii) 

	for iii in lst2:
		if iii not in lst1:
			print "Failed. ", iii, " not in original substring list"
			sys.exit(0)
			
	print "Pass. Original substring list contains all substrings"

hasall('0001020304')
hasall('0001020405')


'''
for i in range 0, len(s) in multiples of n
# 1st pass: s[0:2], then s[2:4] ..
# OR lst.append(s[i:i + n]) - to simply these 2 lines: out = s[i:i + n], lst1.append(out)
# Sample output ['09', '45', '71', '92']

# Adding leading 0 if less than 2 digits (%02d=01, %03d=001)
'''

