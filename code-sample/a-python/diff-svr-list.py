#!/usr/bin/python

# sometimes diff can be a pain to see what is missing from 2 files
# this is a quick hack to take 2 files into a list and check if item from list1 is in list2
# when dealing with 1000s of servers, some servers may not be up when you are performing an installation or verification
# i like to use pdsh to perform the task and have 2 output files to compare ex: pdsh -w ^live-svrs 'dpkg -l |grep -i jinja2' > file1
# comparison should give potential servers that could have been down during 1st installation

all_servers="file1"
live_servers="file2"
lst1 = [line.rstrip('\n') for line in open(all_servers)]
lst2 = [line.rstrip('\n') for line in open(live_servers)]

for item in lst1:
    if item not in lst2:
        print item # or append to a list or another file


# Also modules to do the same ex:
# print(list(set(lst1) - set(lst2))) # or print(DeepDiff(lst1, lst2))
 
