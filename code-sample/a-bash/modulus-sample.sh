#!/bin/bash

# Very simple example that can be used to set various things depending on what given value and modulus is great for it

# Cron example setting min btw 0-59
min=$(( 0x$(hostid) %60 ))
echo "${min} * * * * /usr/local/bin/myscript " > /etc/cron.d/myscript
chmod 0644 /etc/cron.d/myscript

# Given a list of hosts, setup /etc/resolv.conf nameserver in different order so you can load balance DNS between
# 3 Nameservers. This is good so that all clients don't always go to Nameserver 1 

result=$(( 0x$(hostid) %3 ))
echo "Modulus result: ${result}"

if [[ ${result} == 0 ]]; then
    echo "Using Nameserver 0 first"
elif [[ ${result} == 1 ]]; then
    echo "Using Nameserver 1 first"
else 
    echo "Using Nameserver 2 first"
fi

