#!/usr/bin/python 

# Assuming a postgres DB with some random tables that include ticket ids, status and time fields,
# this sample script is an example of logging in to local DB and updating a table with some values

import datetime
import time
import subprocess
import os

login = ["psql", "-h", "127.0.0.1", "-p", "<port#>", "-U", "<user>", "-d", "<db_name>"]

days_in_secs_30 = 123456 
today_minus_30_days_ago =  time.time() - days_in_secs_30
print  today_minus_30_days_ago
closed = "'Closed'"

login.extend(('-c', 'update ticket_field set status=%s where time_t_field > %s;' % (closed, today_minus_30_days_ago)))
p1 = subprocess.Popen(login, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
svnout1, err = p1.communicate()
print svnout1 + '\n'
print err


login.extend(('-c', 'select id_field, time_t_field, status_field from ticket_field where time_t_field > %s;' % (today_minus_30_days_ago)))
p2 = subprocess.Popen(login, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
svnout2, err = p2.communicate()
print svnout2 + '\n'
print err
