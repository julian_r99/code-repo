#!/usr/bin/env python3

# Simple python script to parse a json file. json-data.json provided as an example

import json
import sys, getopt

def main(argv):
    inputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi", ["ifile="])
    except getopt.GetoptError:
        print ('json-parser.py --i <inputfile ex: json-data.json>')
        sys.exit(2)

    for opt, arg in opts:
        if (opt, arg) in opts:
            if opt == '-h':
                print ('json-parser.py --i <inputfile ex: json-data.json>')
                sys.exit()
            elif opt in ("-i", "--ifile"):
                inputfile = arg

    with open (inputfile, mode='r', encoding='utf-8') as json_str:
            #load the json to a string
            resp = json.loads(json_str.read())
            #print the resp
            #print (resp)

            #extract an element in the response
            #print (resp['products'][0])
            product="products"
            name="name"
            rev="rev"
            mydict={}
            products_len = (len(resp[product]))
            for i in range(0, products_len):
                mydict[resp[product][i][name]] = (resp[product][i][rev])
            print ("product_version_map:", mydict)

if __name__ == "__main__":
    main(sys.argv[1:])

# Output Sample: product_version_map: {'prod1': '1', 'prod2': '2', 'prod3': '3'}
