#!/usr/bin/python

# This is a simple script to check all links within acme.com and check status code.
# If you are running from a Mac OS X and you may not have 'requests, urllib2, BeautifulSoup'
# you will have to install them. An easy way to install using pip is: sudo pip install requests

import requests
import urllib2
from BeautifulSoup import BeautifulSoup as soup

mylist = []


url = 'http://www.acme.com'
content = urllib2.urlopen(url).read()
html = soup(content)

links = [tag.attrMap['href'] for tag in html.findAll('a', {'href': True})]

for link in links:
	# To get a failed test, I'm removing the google.com link and leaving
	# the last link which starts with: /mailto which will return me a failed status  
	if 'http' not in link:
		mylist.append('http://www.acme.com/' + link)

# Verifying status codes are 200 and printing Pass or Fail
for i in mylist:
	response = requests.get(i)
	if str(response.status_code) == '200':
		print 'PASSED ---> ' + i + ' response code = ' + str(response.status_code)
	else:
		print '*FAILED* ---> ' + i + ' response code = ' + str(response.status_code)



'''
Same output:
	PASSED ---> http://www.acme.com/privacy.html response code = 200
	PASSED ---> http://www.acme.com/donate/ response code = 200
	*FAILED* ---> http://www.acme.com//mailto/?id=wa response code = 400
'''
