resource "tls_private_key" "mydomain_private" {
    algorithm = "RSA"
    ecdsa_curve = "4096"
}

resource "tls_cert_request" "mydomain_cert_request" {
    key_algorithm = "RSA"
    private_key_pem = "${tls_private_key.mydomain_private.private_key_pem}"

    subject {
        common_name = "*.mydomain.prv"
        organization = "My Company"
        organizational_unit = "Dev"
        country = "US"
        province = "CA"
        locality = "San Francisco"
    }
}

resource "tls_locally_signed_cert" "mydomain_cert" {
    cert_request_pem = "${tls_cert_request.mydomain_cert_request.cert_request_pem}"

    ca_key_algorithm = "RSA"
    ca_private_key_pem = "${file("${path.module}/mycompanydevca.key")}"
    ca_cert_pem = "${file("${path.module}/mycompanydevca.crt")}"

    validity_period_hours = 43800

    allowed_uses = [
        "key_encipherment",
        "digital_signature",
        "server_auth",
    ]
}

output "mydomain_cert" {
    value = "${tls_locally_signed_cert.mydomain_cert.cert_pem}"
}

resource "null_resource" "output_files" {
    provisioner "local-exec" {
        command = "echo \"${tls_locally_signed_cert.mydomain_cert.cert_pem}\" > mydomain.crt"
    }
    provisioner "local-exec" {
        command = "echo \"${tls_private_key.mydomain_private.private_key_pem}\" > mydomain.key"
    }
}
