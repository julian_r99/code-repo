#!/bin/bash

# AWS Route53 hosted zones can't be deleted if resource records sets exists

usage() { echo "Usage: $0 <route53-domain-name> (example: $0 mydomain.cloud.com"; exit 2; }

[[ $# -ne 1 ]] && usage

domain_to_delete=$1

# Look up the hosted zone id for the domain
hosted_zone_id=$(
  aws route53 list-hosted-zones \
	      --output text \
	          --query 'HostedZones[?Name==`'$domain_to_delete'.`].Id'
)

# Phoenix setup has 2 hosted zones for each environment 
# Exit if hosted_zone_id is empty
if [[ -z $hosted_zone_id ]]; then
	echo "No Hosted zone id found for $domain_to_delete. Hosted zone ids queried: $hosted_zone_id"
	echo "No Hosted zones to clean."
	exit 0
else
    echo "Hosted zone: $domain_to_delete. Hosted zone ids to be deleted: $hosted_zone_id"
    # Use list-resource-record-sets to find all of the current DNS entries in the hosted zone, 
    # then delete each one with change-resource-record-sets
    for i in $hosted_zone_id;
    do
    	aws route53 list-resource-record-sets --hosted-zone-id $i |
    	jq -c '.ResourceRecordSets[]' |
    	while read -r resourcerecordset; do
    		read -r name type <<<$(jq -r '.Name,.Type' <<<"$resourcerecordset")
    		if [ $type != "NS" -a $type != "SOA" ]; then
    			echo "Deleting Resource Record $type $name" # Print A record and record name
    			change_id=$(aws route53 change-resource-record-sets --hosted-zone-id $i \
    			--change-batch '{"Changes":[{"Action":"DELETE","ResourceRecordSet": '"$resourcerecordset"' }]}' \
    			--output text --query 'ChangeInfo.Id')
    			echo "DELETING: $type $name $change_id"
    		fi
    	done
    	
    	# delete the hosted zone itself
    	change_id=$(aws route53 delete-hosted-zone --id $i --output text --query 'ChangeInfo.Id')
    	echo "DELETING: hosted zone for $domain_to_delete $change_id"
    
    done
fi
