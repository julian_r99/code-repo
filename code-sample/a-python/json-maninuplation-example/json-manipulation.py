import re
import json
import pprint


# Given an incorrect json that has ( ), remove ( ), add new key and values.
# Json is a key followed by value where the value can be a list or a dictonary 
'''
(
  {
    "targets": [ "localhost:9100", "host2:9100", "host3:9100" ],
    "labels": {
      "env": "prod",
      "job": "node"
    }
  }
)
'''

file="prometheus-targets.json"
with open(file, 'r') as f:
    out = f.read()
#print out

out = re.sub('^\(', '', out)
out = re.sub('\)$', '', out)
#print out
jout = json.loads(out)

# adding new key
jout["newlocalhost:9100"]=123

# append to a list from a specific key
jout["targets"].append("newvalue:9100")

# adding new key and value to the value of key called labels
jout["labels"]["newkey"]=456

# modifying a value from single value to a value list
jout["labels"]["env"] = [1,2,3]

pprint.pprint(jout)

print json.dumps(jout)

# Now i can write to a file using json.dumps or i can ADD BACK the [ ] that the program (prometheus) needs to work correctly, then write jout to the same file which will overwrite it

