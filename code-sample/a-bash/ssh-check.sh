#!/bin/bash

# Function for printing usage
usage() { echo "Usage: $0 version (example: <hostname>)"; exit 2; }

# One parameter must be passed
[[ $# -ne 1 ]] && usage

host=$1

# Just doing a count to browse thru the outputs easier
# the Date command from the beginning and end would actual tell us how long it would have taken 
# for the server to have ssh daemon in open status
c=1
while true; 
do
    nmap_output=`nmap ${host} -PN -p ssh | grep open`
    echo "Current status of nmap ${host} -PN -p ssh | grep open"
    echo "Status: ${nmap_output}"
    echo "Count: ${c}"
    date
    
    # Note: assume you have your root ssh pub keys on the destination host
    # Setting ConnectTimeout=1 to only wait 1 sec and not hang waiting if server is not still up
    # It actual takes the ssh command more than 1 sec to try and exit so rely on date command if checking for duration
    sudo ssh -q -o ConnectTimeout=1 ${host} exit
    if [ $? -eq 0 ]; then
        echo "secs: ${i}"
        echo "SUCCESS: SSH returned success"
        exit 0
    else
        echo "SSH Still not avail"
        echo "secs: ${c}"
        ((c++))
        sleep 1
    fi
done
date

