#!/bin/bash

# Simple script to monitor some resources on a given Ubuntu box
# Usage: ./usage-monitoring.sh > monitoring.log 2>&1 &

timeout=3600

while true
do
    date
    top -n1 -b |head -n25
    echo "#########################"
    df -h
    sleep $timeout
done
