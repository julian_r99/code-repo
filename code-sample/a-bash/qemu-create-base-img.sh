#!/bin/bash

# Script offered AS-IS. It was tested on my local server with specific packages, paths and files.
# Use this as a reference only.

THISSCRIPT="$(basename $0)"

function Usage() {
cat << EOF
Script to convert a qemu img into a golden img 

Usage: $THISSCRIPT  [ -i <vm_img_name> ]  [ -n <new_golden_img> ] ...

-i <vm_img_name>    VM Image name (ex: qemu-kvm.img)
-n <new_golden_img>  New Golden image name to be created (ex: my-golden-v01.img)

EOF
exit $1
}

vm_img_name=
new_golden_img=

# Getting ARGVs. :i:  the ':' means following ARGV is required
while getopts ":i:n:" opt; do
    case $opt in
        h) Usage 0
           ;;
        i) vm_img_name="$OPTARG"
           ;;
        n) new_golden_img="$OPTARG"
           ;;
        *) Usage 1
           ;;
    esac
done

# Forcing to have 4 arguments being passed
[[ $# -ne 4 ]] && Usage

base_server_host="${vm_img_name:0:10}"
vm_name="${vm_img_name:0:13}"
echo "Gathering info provided:"
echo "vm_img_name: ${vm_img_name}, new_golden_img: ${new_golden_img}, VM: ${vm_name}, Base Server Host: ${base_server_host}"
echo ""

base_nfs_server=$(sudo ssh ${base_server_host} df |grep ${vm_name} | awk '{print $1}' | awk -F '-' '{print $3}')

execution_server_img_path="/data/${base_nfs_server}-virtual-machines/${base_server_host}/${vm_name}"
execution_server_img="${execution_server_img_path}/${vm_img_name}"

function img_chk() {
    echo "Checking if img: ${vm_img_name} exists"
    ls ${execution_server_img}
    if [ $? -eq 0 ]; then
        echo "img:${execution_server_img} exists"
    else
        echo "Img check: FAIL. Please check img: ${execution_server_img} and path and try again"
        echo "Make sure you are on execution_server"
        exit 2
    fi
    echo ""
}

function vm_shutdown() {
    echo "Shutting down VM:${vm_name} to create golden img"
    echo " Must Check if VM not already shutdown"
    vm_state=$(sudo ssh ${base_server_host} virsh list --all |grep ${vm_name} | awk '{print $3}')
    if [[ ${vm_state} = "running" ]]; then
        sudo ssh ${base_server_host} virsh destroy ${vm_name}
        echo "============================="
        echo "List of VMs after shutdown of VM: ${vm_name}"
        echo "============================="
        sudo ssh ${base_server_host} virsh list --all
        echo "============================="
        echo "VM shutdown success"
        echo ""
        echo "============================="
    else
        echo "ERROR"
        echo "VM: ${vm_name} not running. Current State: ${vm_state}"
        echo "********************************************************"
        echo "***Verify VM exists or have NOT been shutdown already***"
        echo "********************************************************"
        exit 1
    fi
}

function qemu_convert() {
    echo ""
    echo "Converting ${vm_img_name} to ${new_golden_img}"
    echo "Running sudo qemu-img convert -O qcow2 ${execution_server_img} ${execution_server_img_path}/${new_golden_img}"
    sudo qemu-img convert -O qcow2 ${execution_server_img} ${execution_server_img_path}/${new_golden_img}
    if [ $? -eq 0 ]; then
        echo "Convertion Success"
    else
        echo "Convertion FAIL"
        exit 2
    fi
    echo ""
}

function main_copy() {
    echo -e "Copying image ${new_golden_img} to nfs_path1, nfs_path2\n"
    
    declare -a base_nfs_server_lst=("/data/nfs_path1-virtual-machines/.golden-imgs" "/data/nfs_path2-virtual-machines/.golden-imgs") 
    
    for base_nfs_server in "${base_nfs_server_lst[@]}"; do
        sudo cp ${execution_server_img_path}/${new_golden_img} ${base_nfs_server}/    
        if [ $? -eq 0 ]; then
            echo "SUCCESS: Image copy succeeded"
        else
            echo "FAIL: Image copy fail sudo cp ${execution_server_img_path}/${new_golden_img} ${base_nfs_server}/"
            exit 2
        fi
        echo ""
        # Good idea to make images read only
        echo -e "Changing permission of golden image ${new_golden_img} to 0444\n"
        sudo chmod 0444 ${base_nfs_server}/${new_golden_img}
        ls -lah ${base_nfs_server}/${new_golden_img}
    done
    
}

img_chk
vm_shutdown
qemu_convert
main_copy
     
