import requests
import json
import time
import datetime
import sys
from argparse import ArgumentParser

parser = ArgumentParser()
#parser.add_argument('-s','--symbol', nargs='+', default=['TBT', 'UUP', 'SPY', 'USO', 'GLD', 'FXA', 'FXC', 'WEAT','SOYB','CORN','NIB'], help='Stock symbols')
#parser.add_argument('-s','--symbol', nargs='+', default=['WEAT','SOYB','CORN','NIB'], help='Stock symbols')
parser.add_argument('-s','--symbol', nargs='+', default=['WEAT'], help='Stock symbols')
parser.add_argument('-i', '--ind', required=True, help='Indicator json file')
parser.add_argument('-p', '--price', required=True, help='Price json file')
# stocks currently (Jan 2020) with price 30-40
#parser.add_argument('-s','--symbol', nargs='+', default=['ebay', 'khc', 'ppl', 'glw', 'foxa', 'gm', 'bac', 't', 'xp', 'pba', 'stor', 'nrg', 'lk', 'phm', 'rol', 'ally', 'dre', 'apa', 'nyt', 'bhf'], help='Stock symbols')
parser.add_argument('-l', '--loss', type=int, default=2,  help='Stop Loss days ago.')
parser.add_argument('-sl', '--sl_percent', type=float, default=0.02, help='Stop Loss percent ex: 0.02.')
parser.add_argument('-pt', '--pt_percent', type=float, default=0.03, help='Profit Target percent ex: 0.03.')
parser.add_argument('-n', '--number', type=int, default=100, help='Number of shares ')
parser.add_argument('-y', '--backtestyears', nargs='+', default=['2019', '2018', '2017', '2016', '2015', '2014', '2013', '2012', '2011', '2010', '2009'],  help='Backtest years ex: 2019, 2018 ')
#parser.add_argument('-y', '--backtestyears', nargs='+', default=['2019', '2018'],  help='Backtest years ex: 2019, 2018 ')
args = parser.parse_args()

#Symbols=['UUP','FXE','USO','DNO','SPY','SH','IWM','RWM','TBT','TLT','GLD','DGLD','FXA','FXC','WEAT','SOYB','CORN','NIB','JO','SGG']
# python Back_testing_w_close_price-MACD_long.py -s tbt -l 1 -n 280 -y 2019 2018 2017 2016 2015 2014 2013 2012 2011 2010 2009

API_KEY='VQIPSWMCA8NNMT'
Symbols =  args.symbol
sl_daysago = args.loss
shares = args.number
sl_percent = float(args.sl_percent)
pt_percent = float(args.pt_percent)
# check_list is the years to backtest:
check_list = args.backtestyears
indicator_file = args.ind
price_file = args.price

def trigger_scanner(Symbol,API_KEY):
    #res=requests.get('https://www.alphavantage.co/query?function=MACD&symbol='+Symbol+'&fastperiod=10&slowperiod=16&signalperiod=9&interval=daily&series_type=close&apikey='+API_KEY)
    with open(indicator_file) as data_file1:
        res = json.load(data_file1)

        count=0
        data=res['Technical Analysis: MACD']

        ordered_dates=[]
        for date in data:
            ordered_dates.append(date)
        ordered_data={}
        for date in ordered_dates[::-1]:
            ordered_data.update({date:data[date]})
        
        #ordered_data now has 2019-10-15': {'MACD': '0.0458', 'MACD_Signal': '-0.0807', 'MACD_Hist': '0.1264'}
        

        previousK=None
        currentK=None
        previousD=None
        currentD=None
        previousClose=None
        currentClose=None
        
        triggered_dates=[]
        check_for_close_price=[]

        data=ordered_data
        # data now has: 2019-10-15': {'MACD': '0.0458', 'MACD_Signal': '-0.0807', 'MACD_Hist': '0.1264'}
        
        for date in data:
            currentK=float(data[date]['MACD'])
            currentD=float(data[date]['MACD_Signal'])

            if previousK==None:
                previousK=float(data[date]['MACD'])
                previousD=float(data[date]['MACD_Signal'])
                previousDate=date

            if currentK>currentD:
                if previousK<previousD:
                    if currentK>previousK:
                        triggered_dates.append(date)
            # triggered_dates now thave the first crossover where StochK crosses StockD and is greater then the previous crossover
            # triggered_dates: ['2008-07-16'] 
                        
            previousK=currentK
            previousD=currentD
            previousDate=date
            # previousDate: is previous crossover

        Triggered_Dates=[]
        triggeredK=None
        triggered_date=None
        previous_date=None
        for date in triggered_dates:
            currentK=float(data[date]['MACD'])
            
            if not triggeredK==None:
                if currentK>triggeredK:
                   Triggered_Dates.append([date,previous_date,currentK])
                   # print('Triggered_Dates: {}'.format(Triggered_Dates))
                   # Triggered_Dates has: [['2008-08-06', '2008-07-16', 0.0554]] where previous_date is the previous crossover
                   # This has the current crossover where it matches our criteria (currentK > previousK, etc) plus the previous day date) date,ordered_dates[ordered_dates.index(date)+1]
                   # print(date,ordered_dates[ordered_dates.index(date)+1]) +1 is date of 1 day ago
                                        
                   triggeredK=currentK
                else:
                    triggeredK=currentK

            else:
                triggeredK=currentK
            previous_date=date
            # previous_date is just previous crossover date that matched our critiria: 2008-08-06 2008-07-16 <---- this is: previous_date  
       
    # Getting the price data
    #res2=requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+Symbol+"&outputsize=full&apikey="+API_KEY)
    with open(price_file) as data_file2:
        res2 = json.load(data_file2)
        Final_matches=[]
            
        for each_match in Triggered_Dates:
            try:
                data_for_date=res2['Time Series (Daily)']
            except:
                print(res2.items())
            try:
                currentClose=float(data_for_date[each_match[0]]['4. close'])
                currentDate=each_match[0]
                previousDate=each_match[1]
                previousClose=float(data_for_date[previousDate]['4. close'])
                # currentDate,previousDate,previousClose: 2008-08-06 2008-07-16 69.8
                
                
                if currentClose<previousClose:
                    if float(each_match[2])<0: # This is where MACD cross is below 0 for longs
                        # ordered_dates.index(currentDate)+1 <-----
                        # +1 1 day ago, +2 2 days ago, -1 1 day in the future
                        #Final_matches.append([currentDate,ordered_dates[ordered_dates.index(currentDate)+2]])
 
 # =    ===>   # Stoploss  ### Here I can specify date of 1 day ago or more days ago for the stoploss ###
                        Final_matches.append([currentDate,ordered_dates[ordered_dates.index(currentDate)+sl_daysago]])
 # =    ===>   ###  Final_matches now have: currentDate where Divergence is found plus previousDate: [['2008-10-10', '2008-10-09']]
              
            except Exception as e:
                print(e)

        totalProfit=[]
        totalLoss=[]

        try:
            data_for_date=res2['Time Series (Daily)']

            #check_list=['2018','2017']
            #check_list=['2019'] # this is being passed in the command line
            ord_triggered_dates= Final_matches[::-1]
            final_triggers=[]
            
            for year in check_list:
                for date in ord_triggered_dates:
                    if year in date[0]:
                        final_triggers.append(date)
                        # final triggers have: all the price data: '2008-05-22': {'1. open': '70.1800', '2. high': '71.7100', '3. low': '70.1100', '4. close': '70.8900', '5. volume': '160456'}}
                        #print('final_triggers dates trigger date and previous day date:  {}'.format(final_triggers))
                        #print('\n')
            
            WinLossSeq = []
            idx = 1
            totalNumberWin = 0
            totalNumberLoss = 0
            #for each_date in Triggered_Dates[::-1][:3]: # :3 last 3
            for each_date in final_triggers:  # for all

                try:
                    currentClose=float(data_for_date[each_date[0]]['4. close'])
                    #previousLow=float(data_for_date[each_date[1]]['3. low'])
                    previousLow=float(data_for_date[each_date[1]]['3. low'])
                   
                    # Here is how to walk thru the future date to hit profit target
                    next_dates=ordered_dates[::-1][ordered_dates[::-1].index(each_date[0])+1:]
                    
                    #stopLoss = previousLow
                    #profitTarget = (currentClose - previousLow) * 2.0 + currentClose
                    stopLoss =  (currentClose - (currentClose * sl_percent)) 
                    profitTarget = (currentClose + (currentClose * pt_percent))

                    for next_date in next_dates:
                        if float(data_for_date[next_date]['4. close'])>=profitTarget:
                            #good print#print('previousLow ',sl_daysago, ' days ago: ' ,previousLow, 'profitTarget: ',round(float(profitTarget),2),'  currentClose: ',currentClose,'  Exit Close Price: ',float(data_for_date[next_date]['4. close']))                        
                            #good print#print("Profit Hit: triggered date is ",each_date[0],'close price on triggered date',currentClose,'Profit hit on ',next_date, 'close price on profit date',float(data_for_date[next_date]['4. close']))
                            #good print#print('Win Date: ',each_date[0], 'Win Amount: ', round(float(data_for_date[next_date]['4. close'])-currentClose,2))
                            #print("*Profit hit on ",next_date, 'close price on profit date',float(data_for_date[next_date]['4. close']),'triggered date is ',each_date[0],'close price on triggered date',currentClose)
                            totalProfit.append(round(float(data_for_date[next_date]['4. close'])-currentClose,2))
                            WinLossSeq.append('{}. {}'.format(idx, round(float(data_for_date[next_date]['4. close'])-currentClose,2)))
                            idx += 1
                            totalNumberWin += 1
                            print('\n')
                            #data_for_date has values like this: '3. low': '70.3000', '4. close': '71.2500'
                            #print('data_for_date {}'.format(data_for_date))
                            break

                        elif float(data_for_date[next_date]['4. close'])<=stopLoss:
                            #good print#print('previousLow ',sl_daysago, ' days ago: ' ,previousLow,'stopLoss: ',round(float(stopLoss),2),'  currentClose: ',currentClose,'  Exit Close Price: ',float(data_for_date[next_date]['4. close']))
                            #good print print#print("Loss Hit: triggered date is ",each_date[0],'close price on triggered date',currentClose,'Loss hit on ',next_date, 'close price on loss date',float(data_for_date[next_date]['4. close']))
                            #print("*Loss hit on ",next_date, 'close price on loss date',float(data_for_date[next_date]['4. close']),'triggered date is ',each_date[0],'close price on triggered date',currentClose)
                            #good print#print('Loss Date: ',each_date[0], 'Loss Amount: ', round(currentClose-float(data_for_date[next_date]['4. close']),2))
                            totalLoss.append(round(currentClose-float(data_for_date[next_date]['4. close']),2))
                            WinLossSeq.append('{}. {}'.format(idx, round(float(data_for_date[next_date]['4. close'])-currentClose,2)))
                            idx += 1
                            totalNumberLoss += 1
                            print('\n')
                            break

                except Exception as e:
                    print(e)
            
            FinalProfit = (round(sum(totalProfit),2) * shares) 
            FinalLoss = (round(sum(totalLoss),2) * shares)

            print('=============================================================================')
            print('Values specified for backtest: Symbols: {}, \nNumber of shares: {}, \nStop Loss percent: {}, \nProfit Target percent: {}, \nYears to backtest: {}\n'.format(Symbols, shares, sl_percent, pt_percent, check_list)) 

            print('=============================================================================')
            print('Each Profit      :',totalProfit,' ==> total Profit * number of shares:', shares, '         : $'  ,FinalProfit)
            print('Each Loss         :',totalLoss,' ==> total Loss * number of shares:', shares, '       : $-'  ,FinalLoss)
            print('WinLossSeq: ', WinLossSeq)
            print('=============================================================================')
            print('Number of Winners         :',totalNumberWin)
            print('Number of Lossers         :',totalNumberLoss)
            print('Total Number of Trades    :',(totalNumberWin + totalNumberLoss))
         
            #print('total Profit        :'  ,round(sum(totalProfit),2))
            #print('total Loss       :'  ,round(sum(totalLoss),2))

        except:
            print(res2.items())


for Symbol in Symbols:
    print('Data for symbol ', Symbol)
    trigger_scanner(Symbol,API_KEY)
    print('\n\n\n')
    #time.sleep(30)
    
