#!/bin/bash

# simple script to test adding 1 line at time to /etc/group
# good to test of finding sinble bad entry which was causing ssh to fail.
# I had another window running a ssh loop until it succeeded
# You can combine all in one depending on how bad the issue is as 
# bad passwd, group, etc, can cause bad lock downs.
# this allows you to run from different servers if desired

for i in {1..47}; # 47 default lines from Centos 7 /etc/group
do
   t=$(tail -${i} group.good |awk -F ':' '{print $1}')
   tail -${i} group.good |awk '{print $1}' >> group-new
   echo $t
   echo ""
   cat /etc/group |grep -v "${t}:" >> group-new
   cp group-new /etc/group
   echo > group-new
   sleep 5
done
