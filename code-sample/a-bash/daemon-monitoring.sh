#!/bin/sh

# Given a daemon that dies and causes logs to grow big, this script can help monitoring
# Run it in the background /home/juliano/daemon-monitoring.sh > /dev/null 2>&1 &
# It will run every 12 hours (43200 secs)

while true; do
	WORD="server-pattern-name"
	FILE1="output.log"
	echo "Daemon XYZ is dying every so often without any errors" > output.log
	echo "Daemon XYZ has dies and logs are GT 1G, we know we need restart XYZ" >> output.log
	echo "========================================================================" >> output.log
	echo "" >> output.log
	# Below will run du on a list of server and output to a log excluding any lines containing cannot
	dsh -c -M -f list_of_servers -- du -shxt 1G /var/log |grep -v cannot >> output.log
			
	if grep -q "$WORD" "$FILE1"; then
	cat $FILE1 | mail -s 'ALERT - Scan list gt 1G' myemail@domain.com,other-emails@domain.com
	fi
	sleep 43200
done
