#!/bin/sh
diskusage=$(df -h / |grep / | awk '{print $5}' | sed 's/%//g')
limit=80

if [ "$diskusage" -gt "$limit" ] ; then
    echo "$diskusage -gt $limit testing body" | mail -s 'Disk usage report' myemail@domain.com,more@emails.com 

fi
