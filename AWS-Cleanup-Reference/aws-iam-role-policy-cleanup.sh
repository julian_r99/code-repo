#!/bin/bash

# AWS Role Policy Cleanup 
# Reference: http://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_manage_delete.html

usage() { echo "Usage: $0 <DOMAIN_NAME> <AWS_REGION> <ROLE_SUFFIX> (example: $0 bob us-west-2 my_access_role"; exit 2; }

[[ $# -ne 3 ]] && usage

DOMAIN_NAME=$1
AWS_REGION=$2
ROLE_SUFFIX=$3

# I am using them because roles and policy do not show VPC_ID
# To remove the role from an instance profile:
aws iam --region ${AWS_REGION} remove-role-from-instance-profile --instance-profile-name ${DOMAIN_NAME}--${AWS_REGION}--${ROLE_SUFFIX} --role-name ${DOMAIN_NAME}_${AWS_REGION}_${ROLE_SUFFIX}

# Delete all policies that are associated with the role
aws iam --region ${AWS_REGION} list-role-policies --role-name ${DOMAIN_NAME}_${AWS_REGION}_${ROLE_SUFFIX} |grep ${VPC_ID} | sed 's/ //g' | xargs aws iam --region ${AWS_REGION} delete-role-policy --role-name ${DOMAIN_NAME}_${AWS_REGION}_${ROLE_SUFFIX} --policy-name 

# Now we can delete the Role:
aws iam --region ${AWS_REGION} delete-role --role-name ${DOMAIN_NAME}_${AWS_REGION}_${ROLE_SUFFIX}

# Now we can also delete instance profile that we removed from the role previous
aws iam --region ${AWS_REGION} list-instance-profiles-for-role --role-name ${DOMAIN_NAME}_${AWS_REGION}_${ROLE_SUFFIX} | grep InstanceProfileName | awk '{print $2}' | sed 's/,//g' | xargs aws iam --region ${AWS_REGION} delete-instance-profile --instance-profile-name 

