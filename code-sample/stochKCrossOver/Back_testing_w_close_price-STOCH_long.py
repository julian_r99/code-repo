import requests
import json
import time
import datetime


API_KEY='VQIPSWMCA8NNMTJRXC'
Symbols=['gld']


def trigger_scanner(Symbol,API_KEY):
    res=requests.get('https://www.alphavantage.co/query?function=STOCH&symbol='+Symbol+'&fastkperiod=14&slowkperiod=3&slowdperiod=3&interval=daily&apikey='+API_KEY)

    count=0
    data=res.json()['Technical Analysis: STOCH']

    ordered_dates=[]
    for date in data:
        ordered_dates.append(date)
    ordered_data={}
    for date in ordered_dates[::-1]:
        ordered_data.update({date:data[date]})
        
        

    previousK=None
    currentK=None
    previousD=None
    currentD=None
    previousClose=None
    currentClose=None
    
    triggered_dates=[]
    check_for_close_price=[]

    data=ordered_data

    for date in data:
        currentK=float(data[date]['SlowK'])
        currentD=float(data[date]['SlowD'])

        if previousK==None:
            previousK=float(data[date]['SlowK'])
            previousD=float(data[date]['SlowD'])
            previousDate=date

        if currentK>currentD:
            if previousK<previousD:
                if currentK>previousK:
                    triggered_dates.append(date)
                    
        previousK=currentK
        previousD=currentD
        previousDate=date

    Triggered_Dates=[]
    triggeredK=None
    triggered_date=None
    previous_date=None
    for date in triggered_dates:
        currentK=float(data[date]['SlowK'])
        
        if not triggeredK==None:
            if currentK>triggeredK:
               Triggered_Dates.append([date,previous_date,currentK])
               #print(date,ordered_dates[ordered_dates.index(date)+1])
               #time.sleep(100)
                                    
               triggeredK=currentK
            else:
                triggeredK=currentK

        else:
            triggeredK=currentK

        previous_date=date

    res2=requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+Symbol+"&outputsize=full&apikey="+API_KEY)
    


    Final_matches=[]

        
    for each_match in Triggered_Dates:
        try:
            data_for_date=res2.json()['Time Series (Daily)']
        except:
            print(res2.json().items())

        try:
            currentClose=float(data_for_date[each_match[0]]['4. close'])
            currentDate=each_match[0]
            previousDate=each_match[1]
            #print(currentDate,previousDate)
            previousClose=float(data_for_date[previousDate]['4. close'])
            
            if currentClose<previousClose:
                if float(each_match[2])<60:
                    # +1 1 day ago, +2 2 days ago, -1 1 day in the future
                    #Final_matches.append([currentDate,ordered_dates[ordered_dates.index(currentDate)+2]])
                    Final_matches.append([currentDate,ordered_dates[ordered_dates.index(currentDate)+1]])
        
          
            
        except Exception as e:
            print(e)

    totalProfit=[]
    totalLoss=[]

    try:
        data_for_date=res2.json()['Time Series (Daily)']

        #check_list=['2018','2017']
        check_list=['2018']

        ord_triggered_dates= Final_matches[::-1]

        final_triggers=[]
        
        for year in check_list:
            for date in ord_triggered_dates:
                if year in date[0]:
                    final_triggers.append(date)
                    # printing trigger date and previous day date
                    print('Printing trigger date and previous day date')
                    print(date)
        print('\n')
        

        #for each_date in Triggered_Dates[::-1][:3]: # :3 last 3
        for each_date in final_triggers:  # for all
            

            try:
                currentClose=float(data_for_date[each_date[0]]['4. close'])
                previousLow=float(data_for_date[each_date[1]]['3. low'])
                
    
                next_dates=ordered_dates[::-1][ordered_dates[::-1].index(each_date[0])+1:]

                
                stopLoss = previousLow
                profitTarget = (currentClose - previousLow) * 2.0 + currentClose

                for next_date in next_dates:
                    if float(data_for_date[next_date]['4. close'])>=profitTarget:
                        print('previousLow: ',previousLow, 'profitTarget: ',round(float(profitTarget),2),'  currentClose: ',currentClose,'  Exit Close Price: ',float(data_for_date[next_date]['4. close']))                        
                        print("Profit Hit: triggered date is ",each_date[0],'close price on triggered date',currentClose,'Profit hit on ',next_date, 'close price on profit date',float(data_for_date[next_date]['4. close']))
                        #print("*Profit hit on ",next_date, 'close price on profit date',float(data_for_date[next_date]['4. close']),'triggered date is ',each_date[0],'close price on triggered date',currentClose)
                        totalProfit.append(round(float(data_for_date[next_date]['4. close'])-currentClose,2))
                        print('\n')
                        break

                    elif float(data_for_date[next_date]['4. close'])<=stopLoss:
                        print('previousLow: ',previousLow,'stopLoss: ',round(float(stopLoss),2),'  currentClose: ',currentClose,'  Exit Close Price: ',float(data_for_date[next_date]['4. close']))
                        print("Loss Hit: triggered date is ",each_date[0],'close price on triggered date',currentClose,'Loss hit on ',next_date, 'close price on loss date',float(data_for_date[next_date]['4. close']))
                        #print("*Loss hit on ",next_date, 'close price on loss date',float(data_for_date[next_date]['4. close']),'triggered date is ',each_date[0],'close price on triggered date',currentClose)
                        totalLoss.append(round(currentClose-float(data_for_date[next_date]['4. close']),2))
                        print('\n')
                        break

                    
            
            except Exception as e:
                print(e)


        print('Each Profit      :',totalProfit,' ==> total Profit        :'  ,round(sum(totalProfit),2))
        print('Each Loss         :',totalLoss,' ==> total Loss        :'  ,round(sum(totalLoss),2))

        #print('total Profit        :'  ,round(sum(totalProfit),2))
        #print('total Loss       :'  ,round(sum(totalLoss),2))

    except:
        print(res2.json().items())
        


for Symbol in Symbols:
    print('Data for symbol ', Symbol)
    
    trigger_scanner(Symbol,API_KEY)

    print('\n\n\n')
    
