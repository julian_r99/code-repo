#!/bin/bash

host=$1

# Function for printing usage
usage() { echo "Usage: $0 <hostname>"; exit 2; }

# One parameter must be passed
[[ $# -ne 1 ]] && usage

sudo ssh ${host} ls
while [ $? -ne 0 ]; do
    sleep 2
    echo ""
    echo "ssh still failing. Trying again"
    sudo ssh ${host} ls
done 



