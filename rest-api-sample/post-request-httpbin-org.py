#!/usr/bin/python

# This is a simple script to submit a Post request 
# If you are running from a Mac OS X and you may not have 'requests'
# you will have to install them. An easy way to install using pip is: sudo pip install requests

import requests

url = 'http://httpbin.org/post'

payload = {'key1': 'value1', 'key2': 'value2'}
r = requests.post(url, data=payload)

if str(r.status_code) == '200':
	print 'PASS --> Post Request to ' + url + ' returned ' + str(r.status_code)
else:
	print 'FAIL --> Post Request to ' + url + ' returned ' + str(r.status_code)


'''
Output example:
	PASS --> Post Request to http://httpbin.org/post returned 200
'''

