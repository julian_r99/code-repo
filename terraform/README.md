README
======

Creating Self-Signed Certs with Terraform
-----------------------------------------

Assuming you already have terraform software installed on your Mac or Linux server, the contents of this Readme will walk you thru generating self signed certs with Terraform. 

File 1: signed-certs-and-ca-generation.tf-1 will generate a new CA which is then used to sign the certs.

File 2: self-signed-certs-w-ca-files.tf-2 assuming you only want to create the CA once and use it to sign all other certs, you can run the first command which will create the crt and key (mycompanydevca.crt & mycompanydevca.key) which are them reference on this file (line 24 & 25).

Example
-------

```
Example 1:
mv signed-certs-and-ca-generation.tf-1 signed-certs-and-ca-generation.tf
terraform plan
terraform apply
terraform destroy --force   (This is the clean up)
2 files are generated: mydomain.crt & mydomain.key that can be put into your servers
2 CAs generated that can be used with the next command. (mycompanydevca.crt & mycompanydevca.key).

Example 2:
mv signed-certs-and-ca-generation.tf signed-certs-and-ca-generation.tf-1
mv self-signed-certs-w-ca-files.tf-2 self-signed-certs-w-ca-files.tf
terraform plan
terraform apply
terraform destroy --force    (This is the clean up)
2 files are generated: mydomain.crt & mydomain.key that can be put into your servers.

Note: All your servers will need to have the mycompanydevca.crt installed and updated so they can all trust each other.
Example:
http://manpages.ubuntu.com/manpages/trusty/man8/update-ca-certificates.8.html

